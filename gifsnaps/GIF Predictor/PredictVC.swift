//
//  PredictVC.swift
//  GIF Predictor
//
//  Created by Altynbek Orumbayev on 31/01/2017.
//  Copyright © 2017 A_Orumbayev. All rights reserved.
//

import UIKit
import PureLayout

class PredictVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var firstGif: UIImageView!
    @IBOutlet var secondGif: UIImageView!
    @IBOutlet var thirdGif: UIImageView!
    @IBOutlet var fourthGif: UIImageView!
    @IBOutlet var answerLabel: UITextField!
    
    var selectedGifs = [UIImage]()
    var logoLabel = LTMorphingLabel()
    var backBtn = UIButton()
    var nextBtn = UIButton()
    var didUpdateConstraints = false
    var answer = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        logoLabel = LTMorphingLabel.init(frame: CGRect(x: 50, y: 25, width: SCREEN_WIDTH - 100, height: 50))
        logoLabel.text = "Pick 4 Gifs"
        logoLabel.morphingEffect = .scale
        logoLabel.numberOfLines = 2;
        logoLabel.adjustsFontSizeToFitWidth = true
        logoLabel.textAlignment = .center
        logoLabel.morphingDuration = 1
        logoLabel.font = UIFont(name: "AvenirNext-UltraLight", size: 20)
        logoLabel.textColor = UIColor.white
        self.view.addSubview(logoLabel)
        // Do any additional setup after loading the view.
        
        answerLabel.delegate = self
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        self.backBtn.setImage(UIImage.init(named: "back")?.scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.backBtn.imageView?.contentMode = .scaleAspectFit
        self.backBtn.tintColor = UIColor.white
        self.backBtn.addTarget(self, action: #selector(backBtnPressed), for: .touchUpInside)
        self.view.addSubview(backBtn)
        
        self.nextBtn.setImage(UIImage.init(named: "replay")?.scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.nextBtn.imageView?.contentMode = .scaleAspectFit
        self.nextBtn.tintColor = UIColor.white
        self.nextBtn.alpha = 0.0
        self.nextBtn.addTarget(self, action: #selector(nextBtnPressed), for: .touchUpInside)
        self.view.addSubview(nextBtn)
        
        
        startDetailsNotification()
        
        self.updateViewConstraints()
    }
    
    
    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func nextBtnPressed() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func updateViewConstraints() {
        
        if (!didUpdateConstraints) {
            
            self.backBtn.autoPinEdge(toSuperviewEdge: .left, withInset: 15)
            self.backBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))
            self.backBtn.autoAlignAxis(.horizontal, toSameAxisOf: self.logoLabel)
            
            self.nextBtn.autoPinEdge(toSuperviewEdge: .right, withInset: 15)
            self.nextBtn.autoPinEdge(toSuperviewEdge: .bottom, withInset: 25)
            self.nextBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))
            
            self.didUpdateConstraints = true
        }
        
        super.updateViewConstraints()
    }
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            //  let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            // let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                
                UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                    self.view.frame.origin.y = 0
                    
                }, completion:nil)
                
            } else {
                UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                    self.view.frame.origin.y -= (endFrame?.height)!
                    
                }, completion:nil)
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let imageViews = [firstGif, secondGif, thirdGif, fourthGif]
        for i in 0...3 {
            if let curImageView = imageViews[i]  {
                curImageView.image = self.selectedGifs[i]
            }
        }
    }
    
    func startDetailsNotification() {
        self.logoLabel.text = "Try to predict a word of your opponent"
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.logoLabel.text = "Enter your answer below"
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.logoLabel.text = "Tap on replay to start your turn"
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.startDetailsNotification()
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        if ((textField.text?.replacingOccurrences(of: " ", with: "").characters.count)! <= 0) {
            textField.text = "Enter your answer"
        }
        else {
            UIView.animate(withDuration: 0.2, animations: { 
                textField.alpha = 0.0
            }, completion: { (status) in
                if status {
                    
                    textField.text = "Answer : '\(self.answer)'"
                    textField.isUserInteractionEnabled = false
                    UIView.animate(withDuration: 0.2, animations: {
                        textField.alpha = 1.0
                        self.nextBtn.alpha = 1.0
                    }, completion:nil)
                }
            })
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}
