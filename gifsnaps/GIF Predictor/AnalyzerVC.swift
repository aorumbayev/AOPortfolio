//
//  AnalyzerVC.swift
//  GIF Predictor
//
//  Created by Altynbek Orumbayev on 29/01/2017.
//  Copyright © 2017 A_Orumbayev. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import PureLayout

class AnalyzerVC: UIViewController, AnalyzeImageDelegate, UITextFieldDelegate {
    
    fileprivate var textArray = [
        "Step 1", "Wait until image is being identified", "Pick 4 gifs", "Tap on arrow to continue!"
    ]
    var logoLabel = LTMorphingLabel()
    var analyzedLabel = UITextField()
    var imageView : GPCircularImageView!
    var imageToAnalyze = UIImage()
    var didUpdateConstraints = false
    var backBtn = UIButton()
    var nextBtn = UIButton()
    var activityView : NVActivityIndicatorView!
    var morphingArrayAnimationCounter = 0
    var collectionView: UICollectionView!
    var datasource: JMCFlexibleCollectionViewDataSource?
    var stringOfNouns = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        analyzeImage()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func updateViewConstraints() {
        
        if (!didUpdateConstraints) {
            
            self.backBtn.autoPinEdge(toSuperviewEdge: .left, withInset: 15)
            self.backBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))
            self.backBtn.autoAlignAxis(.horizontal, toSameAxisOf: self.logoLabel)
            
            self.nextBtn.autoPinEdge(toSuperviewEdge: .right, withInset: 15)
            self.nextBtn.autoPinEdge(toSuperviewEdge: .bottom, withInset: 25)
            self.nextBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))
            
            
            self.didUpdateConstraints = true
        }
        
        super.updateViewConstraints()
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            //  let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            // let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                
                UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                    self.imageView.layer.position = CGPoint(x:SCREEN_WIDTH/2, y: SCREEN_HEIGHT/2)
                    self.analyzedLabel.layer.position = CGPoint(x: 0, y: self.imageView.layer.position.y + self.imageView.frame.size.height/2 + self.analyzedLabel.frame.size.height  + 15)
                }, completion:nil)
                
            } else {
                UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                    self.imageView.layer.position = CGPoint(x: self.imageView.layer.position.x, y: self.logoLabel.layer.position.y + self.logoLabel.frame.size.height/2 + self.imageView.frame.size.height/2 + 15)
                    self.analyzedLabel.layer.position = CGPoint(x: self.analyzedLabel.layer.position.x, y: self.imageView.layer.position.y + self.imageView.frame.size.height/2 + 10)
                }, completion:nil)
            }
        }
    }
    
    // MARK: - Private
    
    func setupViews() {
        // Setting up Gradient View
        let gradientView = GPGradientView(frame: self.view.bounds)
        self.view.addSubview(gradientView)
        
        logoLabel = LTMorphingLabel.init(frame: CGRect(x: 50, y: 25, width: SCREEN_WIDTH - 100, height: 50))
        logoLabel.text = "Let's start!"
        logoLabel.morphingEffect = .scale
        logoLabel.numberOfLines = 2;
        logoLabel.morphingEffect = .scale
        logoLabel.adjustsFontSizeToFitWidth = true
        logoLabel.textAlignment = .center
        logoLabel.morphingDuration = 1
        logoLabel.font = UIFont(name: "AvenirNext-UltraLight", size: 20)
        logoLabel.textColor = UIColor.white
        self.view.addSubview(logoLabel)
        
        imageView = GPCircularImageView(frame: CGRect(x: 0, y: 0, width:CIRCULAR_IMAGE_VIEW_SIZE, height:CIRCULAR_IMAGE_VIEW_SIZE), image: imageToAnalyze)
        imageView.layer.position = CGPoint(x: SCREEN_WIDTH/2, y: SCREEN_HEIGHT/2)
        imageView.transform = CGAffineTransform.init(scaleX: 0.1, y: 0.1)
        imageView.alpha = 0.0
        
        analyzedLabel = UITextField.init(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 40))
        analyzedLabel.text = ""
        analyzedLabel.adjustsFontSizeToFitWidth = true
        analyzedLabel.textAlignment = .center
        analyzedLabel.font = UIFont(name: "AvenirNext-UltraLight", size: 28)
        analyzedLabel.textColor = UIColor.white
        analyzedLabel.returnKeyType = .done
        analyzedLabel.alpha = 0.0
        analyzedLabel.delegate = self
        analyzedLabel.keyboardAppearance = .dark
        analyzedLabel.clearButtonMode = .whileEditing
        analyzedLabel.layer.anchorPoint = CGPoint.zero
        analyzedLabel.layer.position = CGPoint(x: 0, y: self.imageView.layer.position.y + CIRCULAR_IMAGE_VIEW_SIZE/2 + self.analyzedLabel.frame.size.height  + 15)
        self.view.addSubview(analyzedLabel)
        
        activityView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width:50, height:50), type: NVActivityIndicatorType.orbit, color: UIColor.white, padding: 0)
        activityView.layer.position = CGPoint(x: SCREEN_WIDTH/2, y: SCREEN_HEIGHT/2)
        self.view.addSubview(activityView)
        activityView.startAnimating()
        
        self.view.addSubview(imageView)
        
        self.backBtn.setImage(UIImage.init(named: "back")?.scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.backBtn.imageView?.contentMode = .scaleAspectFit
        self.backBtn.tintColor = UIColor.white
        self.backBtn.addTarget(self, action: #selector(backBtnPressed), for: .touchUpInside)
        self.view.addSubview(backBtn)
        
        self.nextBtn.setImage(UIImage.init(named: "next")?.scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.nextBtn.imageView?.contentMode = .scaleAspectFit
        self.nextBtn.tintColor = UIColor.white
        self.nextBtn.alpha = 0.0
        self.nextBtn.addTarget(self, action: #selector(nextBtnPressed), for: .touchUpInside)
        self.view.addSubview(nextBtn)
        
        self.updateViewConstraints()
    }
    
    func analyzeImage() {
        let analyzeImage = CognitiveServices.sharedInstance.analyzeImage
        analyzeImage.delegate = self
        self.logoLabel.text = "Wait until image is being identified"
        
        let visualFeatures: [AnalyzeImage.AnalyzeImageVisualFeatures] = [.Categories, .Description, .Faces, .ImageType, .Color, .Adult]
        let requestObject: AnalyzeImageRequestObject = (imageView.image!, visualFeatures)
        
        try! analyzeImage.analyzeImageWithRequestObject(requestObject, completion: { (response) in
            DispatchQueue.main.async(execute: {
                self.performAppearanceAnimationForReponse(response: response?.descriptionText)
            })
        })
    }
    
    func performAppearanceAnimationForReponse(response: String?) {
        if response != nil  {
            
            let options = NSLinguisticTagger.Options.omitWhitespace.rawValue | NSLinguisticTagger.Options.joinNames.rawValue
            let tagger = NSLinguisticTagger(tagSchemes: NSLinguisticTagger.availableTagSchemes(forLanguage: "en"), options: Int(options))
            
            let inputString = response!
            tagger.string = inputString
            
            let range = NSRange(location: 0, length: inputString.utf16.count)
            stringOfNouns = ""
            tagger.enumerateTags(in: range, scheme: NSLinguisticTagScheme.nameTypeOrLexicalClass, options: NSLinguisticTagger.Options(rawValue: options)) { tag, tokenRange, sentenceRange, stop in
                let token = (inputString as NSString).substring(with: tokenRange)
                if (tag?.rawValue.lowercased() == "noun") {
                    self.stringOfNouns.append(" \(token)")
                }
            }
            
            
            self.analyzedLabel.text = response?.capitalized
            self.startDetailsNotification()
            
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                self.analyzedLabel.alpha = 1.0
                self.activityView.stopAnimating()
                self.imageView.transform = CGAffineTransform.identity
                self.imageView.alpha = 1.0
                self.nextBtn.alpha = 1.0
            }, completion: nil)
        }
    }
    
    func startDetailsNotification() {
        self.logoLabel.text = "Check if image identified correctly"
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.logoLabel.text = "Tap on text if you want to correct it"
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.logoLabel.text = "Tap on arrow to continue"
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.startDetailsNotification()
                }
            }
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func nextBtnPressed() {
        
        if (stringOfNouns.replacingOccurrences(of: " ", with: "").characters.count > 0) {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let controller = storyboard.instantiateViewController(withIdentifier: "GifPickVC") as? GIFPickVC {
                controller.textToSearch = stringOfNouns
                controller.answer = self.analyzedLabel.text!
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
            
        else {
            let alert = UIAlertController(title: "Alert", message: "Try different image or enter text manually", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    // MARK: - AnalyzeImageDelegate
    
    func finnishedGeneratingObject(_ analyzeImageObject: AnalyzeImage.AnalyzeImageObject) {
    }
    
    
    // MARK: - Textfield
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text {
            if (text.characters.count > 0) {
                self.stringOfNouns = textField.text!
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    // MARK: - CollectionView Delegate & Datasource
    
    
    
}
