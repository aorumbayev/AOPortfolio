//
//  TutorialView.swift
//  Awardy
//
//  Created by Altynbek Orumbaev on 25.01.16.
//  Copyright © 2016 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import RazzleDazzle

class TutorialVC: AnimatedPagingScrollViewController {
    
    private let page1Logo = UIImageView(image: UIImage(named: "Slide_1"))
    private let page2Logo = UIImageView(image: UIImage(named: "Slide_2"))
    private let page3Logo = UIImageView(image: UIImage(named: "Slide_3"))
    private let page4Logo = UIImageView(image: UIImage(named: "Slide_4"))
    private let page1Text = UIImageView(image: UIImage(named: "logo"))
    private let page1TextLabel = UILabel()
    private let page2Text = UILabel()
    private let page3Text = UILabel()
    private let page4Text = UILabel()
    private let loginBtn = UIButton()
    private let skipBtn = UIButton()
    
    override func numberOfPages() -> Int {
        // Tell the scroll view how many pages it has
        return 4
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gradientView = GPGradientView(frame: self.view.bounds)
        self.view.insertSubview(gradientView, at: 0)
        
        configureViews()
  //      configureScrollView()
        configureFirstPageElements()
        configureSecondPageElements()
        configureThirdPageElements()
        configureFourthPageElements()
        animateCurrentFrame()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func configureViews () {
        // Add each of the views to the contentView
        contentView.addSubview(page1Logo)
        contentView.addSubview(page1Text)
        contentView.addSubview(page1TextLabel)
        contentView.addSubview(page2Text)
        contentView.addSubview(page2Logo)
        contentView.addSubview(page3Logo)
        contentView.addSubview(page3Text)
        contentView.addSubview(page4Logo)
        contentView.addSubview(page4Text)
        contentView.addSubview(loginBtn)
        contentView.addSubview(skipBtn)
        
        page1Text.contentMode = .scaleAspectFit
        page1Logo.contentMode = .scaleAspectFit
        
        page1TextLabel.textColor = UIColor.white
        page1TextLabel.font = UIFont(name: "AvenirNext", size: 25)
        page1TextLabel.numberOfLines = 2;
        page1TextLabel.text = "Step 1.\nSnap a photo with camera or pick an image"
        page1TextLabel.adjustsFontSizeToFitWidth = true
        page1TextLabel.textAlignment = NSTextAlignment.center
        
        page2Logo.contentMode = .scaleAspectFit
        
        page2Text.textColor = UIColor.white
        page2Text.font = UIFont(name: "AvenirNext", size: 25)
        page2Text.numberOfLines = 2;
        page2Text.text = "Step 2.\nWait until objects on image will be identified"
        page2Text.adjustsFontSizeToFitWidth = true
        page2Text.textAlignment = NSTextAlignment.center
        
        page3Logo.contentMode = .scaleAspectFit
        
        page3Text.textColor = UIColor.white
        page3Text.font = UIFont(name: "AvenirNext", size: 25)
        page3Text.numberOfLines = 3;
        page3Text.text = "Step 3.\nPick 4 GIFs that describe your object"
        page3Text.adjustsFontSizeToFitWidth = true
        page3Text.textAlignment = NSTextAlignment.center
        
        page4Logo.contentMode = .scaleAspectFit
        
        page4Text.textColor = UIColor.white
        page4Text.font = UIFont(name: "AvenirNext", size: 25)
        page4Text.numberOfLines = 3;
        page4Text.text = "Step 4.\nGive your device to opponent and let him predict the word 😀"
        page4Text.adjustsFontSizeToFitWidth = true
        page4Text.textAlignment = NSTextAlignment.center
        
        loginBtn.setTitle("Let's Start!", for: .normal)
        loginBtn.setTitleColor(UIColor.white, for: .normal)
        loginBtn.titleLabel!.font =  UIFont(name: "AvenirNext", size: 20)
        loginBtn.layer.cornerRadius = 5
        loginBtn.setTitleColor(UIColor.lightGray, for: .highlighted)
        loginBtn.backgroundColor = UIColor(red: 53/255, green: 57/255, blue: 78/255, alpha: 1)
        loginBtn.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        loginBtn.addTarget(self, action: #selector(skipBtnPressed(_:)), for: .touchUpInside)
    }
    
    private func configureFirstPageElements() {
        // Center the star on the page, and keep it centered on pages 0 and 1
        
        let top = NSLayoutConstraint(item: self.page1Text,
                                     attribute: .top,
                                     relatedBy: .equal,
                                     toItem: self.scrollView,
                                     attribute: .top,
                                     multiplier: 1.0,
                                     constant: 25.0);
        
        let height = NSLayoutConstraint(item: self.page1Text,
                                        attribute: .height,
                                        relatedBy: .equal,
                                        toItem: nil,
                                        attribute: .notAnAttribute,
                                        multiplier: 0.0,
                                        constant: 65.0);
        
        let width = NSLayoutConstraint(item: self.page1Text,
                                       attribute: .width,
                                       relatedBy: .equal,
                                       toItem: nil,
                                       attribute: .notAnAttribute,
                                       multiplier: 0.0,
                                       constant: UIScreen.main.bounds.width - 40);
        
        NSLayoutConstraint.activate([width, height, top])
        
        keepView(page1Text, onPage: 0)
        
        let topIphoneIcon = NSLayoutConstraint(item: self.page1Logo,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: self.page1Text,
                                               attribute: .bottom,
                                               multiplier: 1.0,
                                               constant: 25.0);
        
        
        let leftIphoneIcon = NSLayoutConstraint(item: self.page1Logo,
                                                attribute: .left,
                                                relatedBy: .equal,
                                                toItem: self.page1Text,
                                                attribute: .left,
                                                multiplier: 1.0,
                                                constant: -20.0);
        
        let rightIphoneIcon = NSLayoutConstraint(item: self.page1Logo,
                                                 attribute: .right,
                                                 relatedBy: .equal,
                                                 toItem: self.page1Text,
                                                 attribute: .right,
                                                 multiplier: 1.0,
                                                 constant: 20.0);
        
        let bottomIphoneIcon = NSLayoutConstraint(item: self.page1Logo,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: scrollView,
                                                  attribute: .bottom,
                                                  multiplier: 1.0,
                                                  constant: -100.0);
        
        
        NSLayoutConstraint.activate([topIphoneIcon, leftIphoneIcon, rightIphoneIcon, bottomIphoneIcon])
        keepView(page1Logo,  onPage: 0)
        
        
        let topIphoneIconLabel = NSLayoutConstraint(item: self.page1TextLabel,
                                                    attribute: .top,
                                                    relatedBy: .equal,
                                                    toItem: self.page1Logo,
                                                    attribute: .bottom,
                                                    multiplier: 1.0,
                                                    constant: 5.0);
        
        let leftIphoneIconLabel = NSLayoutConstraint(item: self.page1TextLabel,
                                                     attribute: .left,
                                                     relatedBy: .equal,
                                                     toItem: self.page1Text,
                                                     attribute: .left,
                                                     multiplier: 1.0,
                                                     constant: 0.0);
        
        let rightIphoneIconLabel = NSLayoutConstraint(item: self.page1TextLabel,
                                                      attribute: .right,
                                                      relatedBy: .equal,
                                                      toItem: self.page1Text,
                                                      attribute: .right,
                                                      multiplier: 1.0,
                                                      constant: 0.0);
        
        let bottomIphoneIconLabel = NSLayoutConstraint(item: self.page1TextLabel,
                                                       attribute: .bottom,
                                                       relatedBy: .equal,
                                                       toItem: scrollView,
                                                       attribute: .bottom,
                                                       multiplier: 1.0,
                                                       constant: 5.0);
        
        NSLayoutConstraint.activate([topIphoneIconLabel, bottomIphoneIconLabel, leftIphoneIconLabel, rightIphoneIconLabel])
        keepView(page1TextLabel,  onPage: 0)
        
        let alphaAnimationLogo = AlphaAnimation.init(view: self.page1Text)
        alphaAnimationLogo[0] = 1
        alphaAnimationLogo[0.1] = 0
        alphaAnimationLogo[1] = 0
        animator.addAnimation(alphaAnimationLogo)
        
        let alphaAnimationLogoLabel = AlphaAnimation.init(view: self.page1TextLabel)
        alphaAnimationLogoLabel[0] = 1
        alphaAnimationLogoLabel[0.1] = 0
        alphaAnimationLogoLabel[1] = 0
        animator.addAnimation(alphaAnimationLogoLabel)
        
        let scaleImageAnimation = ScaleAnimation.init(view: page1Logo)
        scaleImageAnimation.addKeyframe(0, value: 1, easing: EasingFunctionEaseInQuad)
        scaleImageAnimation[1] = 5
        animator.addAnimation(scaleImageAnimation)
        
        let leftConstantAnimation = ConstraintConstantAnimation.init(superview: scrollView, constraint: leftIphoneIcon)
        leftConstantAnimation[0] = -20
        leftConstantAnimation[0.75] = 400
        animator.addAnimation(leftConstantAnimation)
        
        let rightConstantAnimation = ConstraintConstantAnimation.init(superview: scrollView, constraint: rightIphoneIcon)
        rightConstantAnimation[0] = 20
        rightConstantAnimation[0.5] = 400
        animator.addAnimation(rightConstantAnimation)
        
        let alphaAnimationIcon = AlphaAnimation.init(view: self.page1Logo)
        alphaAnimationIcon[0] = 1
        alphaAnimationIcon[0.6] = 0
        animator.addAnimation(alphaAnimationIcon)
    }
    
    
    private func configureSecondPageElements() {
        
        NSLayoutConstraint(item: page2Text,
                           attribute: .top,
                           relatedBy: .equal,
                           toItem: self.contentView,
                           attribute: .top,
                           multiplier: 1.0,
                           constant: 25.0).isActive = true
        
        //          NSLayoutConstraint(item: page2Text,
        //                                         attribute: .centerX,
        //                                         relatedBy: .equal,
        //                                         toItem: self.scrollView,
        //                                         attribute: .centerX,
        //                                         multiplier: 1.0,
        //                                         constant: 0).isActive = true
        //
        NSLayoutConstraint(item: page2Text,
                           attribute: .height,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: 65.0).isActive = true
        
        NSLayoutConstraint(item: page2Text,
                           attribute: .width,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: UIScreen.main.bounds.width - 40).isActive = true
        
        keepView(page2Text, onPage:1)
        
        let topIphoneIcon = NSLayoutConstraint(item: page2Logo,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: self.page2Text,
                                               attribute: .bottom,
                                               multiplier: 1.0,
                                               constant: 25.0);
        
        
        let leftIphoneIcon = NSLayoutConstraint(item: page2Logo,
                                                attribute: .left,
                                                relatedBy: .equal,
                                                toItem: self.page2Text,
                                                attribute: .left,
                                                multiplier: 1.0,
                                                constant: -20.0);
        
        let rightIphoneIcon = NSLayoutConstraint(item: page2Logo,
                                                 attribute: .right,
                                                 relatedBy: .equal,
                                                 toItem: self.page2Text,
                                                 attribute: .right,
                                                 multiplier: 1.0,
                                                 constant: 20.0);
        
        let bottomIphoneIcon = NSLayoutConstraint(item: page2Logo,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: scrollView,
                                                  attribute: .bottom,
                                                  multiplier: 1.0,
                                                  constant: -100.0);
        
        NSLayoutConstraint.activate([topIphoneIcon, leftIphoneIcon, rightIphoneIcon, bottomIphoneIcon])
        keepView(page2Logo, onPage:1)
        
        let alphaAnimationLogo = AlphaAnimation.init(view: self.page2Text)
        alphaAnimationLogo[1] = 1
        alphaAnimationLogo[1.1] = 0
        alphaAnimationLogo[2] = 0
        animator.addAnimation(alphaAnimationLogo)
        
        let scaleImageAnimation = ScaleAnimation.init(view: page2Logo)
        scaleImageAnimation.addKeyframe(0, value: 0, easing: EasingFunctionEaseInQuad)
        scaleImageAnimation[0.7] = 0.9
        scaleImageAnimation[0.85] = 1.1
        scaleImageAnimation[1] = 1
        scaleImageAnimation[1.15] = 1.1
        scaleImageAnimation[1.3] = 0.9
        animator.addAnimation(scaleImageAnimation)
        
        let leftConstantAnimation = ConstraintConstantAnimation.init(superview: scrollView, constraint: leftIphoneIcon)
        leftConstantAnimation[1] = -20
        leftConstantAnimation[1.75] = -400
        animator.addAnimation(leftConstantAnimation)
        
     
    }
    
    private func configureThirdPageElements() {
        
        NSLayoutConstraint(item: page3Text,
                           attribute: .top,
                           relatedBy: .equal,
                           toItem: self.contentView,
                           attribute: .top,
                           multiplier: 1.0,
                           constant: 25.0).isActive = true
        
        NSLayoutConstraint(item: page3Text,
                           attribute: .height,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: 65.0).isActive = true
        
        NSLayoutConstraint(item: page3Text,
                           attribute: .width,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: UIScreen.main.bounds.width - 40).isActive = true
        
        keepView(page3Text, onPage:2)
        
        let topIphoneIcon = NSLayoutConstraint(item: page3Logo,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: self.page3Text,
                                               attribute: .bottom,
                                               multiplier: 1.0,
                                               constant: 25.0);
        
        
        let leftIphoneIcon = NSLayoutConstraint(item: page3Logo,
                                                attribute: .left,
                                                relatedBy: .equal,
                                                toItem: self.page3Text,
                                                attribute: .left,
                                                multiplier: 1.0,
                                                constant: -20.0);
        
        let rightIphoneIcon = NSLayoutConstraint(item: page3Logo,
                                                 attribute: .right,
                                                 relatedBy: .equal,
                                                 toItem: self.page3Text,
                                                 attribute: .right,
                                                 multiplier: 1.0,
                                                 constant: 20.0);
        
        let bottomIphoneIcon = NSLayoutConstraint(item: page3Logo,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: scrollView,
                                                  attribute: .bottom,
                                                  multiplier: 1.0,
                                                  constant: -100.0);
        
        NSLayoutConstraint.activate([topIphoneIcon, leftIphoneIcon, rightIphoneIcon, bottomIphoneIcon])
        keepView(page3Logo, onPage:2)
        
        let scaleImageAnimation = ScaleAnimation.init(view: page3Logo)
        scaleImageAnimation.addKeyframe(0, value: 0, easing: EasingFunctionEaseInQuad)
        scaleImageAnimation[1.7] = 0.9
        scaleImageAnimation[1.85] = 1.1
        scaleImageAnimation[2] = 1
        scaleImageAnimation[2.15] = 1.1
        scaleImageAnimation[2.3] = 0.9
        animator.addAnimation(scaleImageAnimation)
    }
    
    
    
    private func configureFourthPageElements() {
        
        NSLayoutConstraint(item: page4Text,
                           attribute: .top,
                           relatedBy: .equal,
                           toItem: self.contentView,
                           attribute: .top,
                           multiplier: 1.0,
                           constant: 25.0).isActive = true
        
        NSLayoutConstraint(item: page4Text,
                           attribute: .height,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: 65.0).isActive = true
        
        NSLayoutConstraint(item: page4Text,
                           attribute: .width,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: UIScreen.main.bounds.width - 40).isActive = true
        
        keepView(page4Text, onPage:3)
        
        let topIphoneIcon = NSLayoutConstraint(item: page4Logo,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: self.page4Text,
                                               attribute: .bottom,
                                               multiplier: 1.0,
                                               constant: 25.0);
        
        
        let leftIphoneIcon = NSLayoutConstraint(item: page4Logo,
                                                attribute: .left,
                                                relatedBy: .equal,
                                                toItem: self.page4Text,
                                                attribute: .left,
                                                multiplier: 1.0,
                                                constant: -20.0);
        
        let rightIphoneIcon = NSLayoutConstraint(item: page4Logo,
                                                 attribute: .right,
                                                 relatedBy: .equal,
                                                 toItem: self.page4Text,
                                                 attribute: .right,
                                                 multiplier: 1.0,
                                                 constant: 20.0);
        
        let bottomIphoneIcon = NSLayoutConstraint(item: page4Logo,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: scrollView,
                                                  attribute: .bottom,
                                                  multiplier: 1.0,
                                                  constant: -100.0);
        
        NSLayoutConstraint.activate([topIphoneIcon, leftIphoneIcon, rightIphoneIcon, bottomIphoneIcon])
        keepView(page4Logo, onPage:3)
        
        let scaleImageAnimation = ScaleAnimation.init(view: page4Logo)
        scaleImageAnimation.addKeyframe(0, value: 0, easing: EasingFunctionEaseInQuad)
        scaleImageAnimation[2.7] = 0.9
        scaleImageAnimation[2.85] = 1.1
        scaleImageAnimation[3] = 1
        scaleImageAnimation[3.15] = 1.1
        scaleImageAnimation[3.3] = 0.9
        animator.addAnimation(scaleImageAnimation)
        
        
        NSLayoutConstraint(item: loginBtn,
                           attribute: .width,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: UIScreen.main.bounds.width - 60).isActive = true
        
        
        NSLayoutConstraint(item: loginBtn,
                           attribute: .height,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: 45).isActive = true
        
        
        let bottomLogin = NSLayoutConstraint(item: loginBtn,
                                             attribute: .bottom,
                                             relatedBy: .equal,
                                             toItem: scrollView,
                                             attribute: .bottom,
                                             multiplier: 1.0,
                                             constant: -20);
        
        NSLayoutConstraint.activate([bottomLogin])
        keepView(loginBtn, onPage: 3, withAttribute: .centerX)
    }

    
    @objc func skipBtnPressed(_ sender: AnyObject) {
        UserDefaults.standard.setValue(true, forKey: "firstLaunch")
         let vc = MenuVC()
        let navVC = UINavigationController.init(rootViewController: vc)
        navVC.setNavigationBarHidden(true, animated: false)
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let snapshot = appDelegate.window?.snapshotView(afterScreenUpdates: true)
            navVC.view.addSubview(snapshot!)
            
            appDelegate.window?.rootViewController = navVC
            UIView.animate(withDuration: 0.25, animations: { 
                snapshot?.layer.opacity = 0
            }, completion: { (status) in
                snapshot?.removeFromSuperview()
            })
        }
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}
