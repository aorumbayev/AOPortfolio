//
//  GPCircularButton.swift
//  GIF Predictor
//
//  Created by Altynbek Orumbayev on 29/01/2017.
//  Copyright © 2017 A_Orumbayev. All rights reserved.
//

import UIKit

class GPCircularButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    convenience init(frame: CGRect, icon: UIImage) {
        self.init(frame: frame)
        self.setImage(icon, for: .normal)
    }
    
    func setupView() {
        self.clipsToBounds = true
        self.layer.borderWidth = 5
        self.imageView?.contentMode = .scaleAspectFit
        self.layer.borderColor = UIColor.init(white: 1.0, alpha: 0.9).cgColor
        self.layer.cornerRadius = CIRCULAR_BTN_SIZE / 2
        self.layer.shadowColor = UIColor(red: SHADOW_COLOR, green: SHADOW_COLOR, blue: SHADOW_COLOR, alpha: 0.5).cgColor
        self.layer.shadowOpacity = 0.8
        self.layer.shadowRadius = 5.0
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        UIView.animate(withDuration: 0.35, delay: 0.0, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform.init(scaleX: 1.1, y: 1.1)
        }, completion: { (status) in
            
        } )
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            super.touchesEnded(touches, with: event)
        }
        
        super.touchesEnded(touches, with: event)
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform.init(scaleX: 0.9, y: 0.9)
        }, completion: { (status) in
            UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
                self.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
            }, completion: { (status) in
                if status {
                }
            } )
        })
        
    }
}
