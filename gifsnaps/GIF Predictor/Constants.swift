//
//  Utils.swift
//  GIF Predictor
//
//  Created by Altynbek Orumbayev on 29/01/2017.
//  Copyright © 2017 A_Orumbayev. All rights reserved.
//

import Foundation
import UIKit

public extension UIImage {
    
    func scaleImage(toSize newSize: CGSize) -> UIImage? {
        let newRect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height).integral
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        if let context = UIGraphicsGetCurrentContext() {
            context.interpolationQuality = .high
            let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
            context.concatenate(flipVertical)
            context.draw(self.cgImage!, in: newRect)
            let newImage = UIImage(cgImage: context.makeImage()!)
            UIGraphicsEndImageContext()
            return newImage
        }
        return nil
    }
}

public extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}

let SHADOW_COLOR: CGFloat = 157.0 / 255.0
let CIRCULAR_BTN_SIZE = (UIScreen.main.bounds.width/2) - 40
let CIRCULAR_IMAGE_VIEW_SIZE : CGFloat = 150
let SCREEN_WIDTH = UIScreen.main.bounds.width
let SCREEN_HEIGHT = UIScreen.main.bounds.height


class Constants {
    static let sharedInstance = Constants()
    
    
    
}
