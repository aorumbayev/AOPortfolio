//
//  GPGradientView.swift
//  GIF Predictor
//
//  Created by Altynbek Orumbayev on 29/01/2017.
//  Copyright © 2017 A_Orumbayev. All rights reserved.
//

import UIKit

class GradientColors {
    static let sharedInstance = GradientColors()
    
    var gradientColors: [CGColor] {
        return [UIColor.init(red: 15/255, green: 1/255, blue: 35/255, alpha: 1.0).cgColor, UIColor.init(red: 37/255, green: 2/255, blue: 23/255, alpha: 1.0).cgColor, UIColor.init(red: 71/255, green: 24/255, blue: 5/255, alpha: 1.0).cgColor, UIColor.init(red: 20/255, green: 7/255, blue: 0/255, alpha: 1.0).cgColor, UIColor.init(red: 46/255, green: 54/255, blue: 72/255, alpha: 1.0).cgColor, UIColor.init(red: 14/255, green: 18/255, blue: 28/255, alpha: 1.0).cgColor];
    }
    
    
    func randomColor() -> [CGColor] {
        let randomIndex1 = Int(arc4random_uniform(UInt32(gradientColors.count)))
        let randomIndex2 = Int(arc4random_uniform(UInt32(gradientColors.count)))
        
        return [gradientColors[randomIndex1], gradientColors[randomIndex2]]
    }
}

class GPGradientView: UIView, CAAnimationDelegate  {
    
    let gradient = CAGradientLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupGradient()
        animateGradient()
    }
    
    func setupGradient() {
        gradient.colors = GradientColors.sharedInstance.randomColor()
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = UIScreen.main.bounds
        
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func animateGradient() {
        
        let animation : CABasicAnimation = CABasicAnimation(keyPath: "colors")
        animation.fromValue = gradient.colors
        let randomColor = GradientColors.sharedInstance.randomColor()
        animation.toValue = randomColor
        animation.duration = 5
        animation.isRemovedOnCompletion = true
        animation.fillMode = kCAFillModeBoth
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        animation.delegate = self
        
        gradient.colors = randomColor
        gradient.add(animation, forKey: "gradientAnimation")
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag {
            animateGradient()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupGradient()
        animateGradient()
    }
    
}
