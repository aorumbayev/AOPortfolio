//
//  GPCircularImageView.swift
//  GIF Predictor
//
//  Created by Altynbek Orumbayev on 29/01/2017.
//  Copyright © 2017 A_Orumbayev. All rights reserved.
//

import UIKit

class GPCircularImageView: UIImageView {

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    convenience init(frame: CGRect, image: UIImage) {
        self.init(frame: frame)
        self.image = image
    }
    
    func setupView() {
        self.clipsToBounds = true
        self.layer.borderWidth = 5
        self.contentMode = .scaleAspectFit
        self.layer.borderColor = UIColor.init(white: 1.0, alpha: 0.9).cgColor
        self.layer.cornerRadius = CIRCULAR_IMAGE_VIEW_SIZE / 2
        self.layer.shadowColor = UIColor(red: SHADOW_COLOR, green: SHADOW_COLOR, blue: SHADOW_COLOR, alpha: 0.5).cgColor
        self.layer.shadowOpacity = 0.8
        self.layer.shadowRadius = 5.0
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


}
