//
//  GIFCollectionViewCell.swift
//  GIF Predictor
//
//  Created by Altynbek Orumbayev on 31/01/2017.
//  Copyright © 2017 A_Orumbayev. All rights reserved.
//

import UIKit

class GIFCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var gifImageView: UIImageView!
    @IBOutlet var tintView: UIView!
    
    override var isSelected: Bool {
        didSet {
            if (isSelected) {
                UIView.animate(withDuration: 0.25, animations: {
                    self.tintView.alpha = 0.65
                })
            }
            else {
                UIView.animate(withDuration: 0.25, animations: {
                    self.tintView.alpha = 0.0
                })
            }
        }
    }
}
