//
//  GIFPickVC.swift
//  GIF Predictor
//
//  Created by Altynbek Orumbayev on 30/01/2017.
//  Copyright © 2017 A_Orumbayev. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Gifu
import Kingfisher
import PureLayout

class GIFPickVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet var collectionView: UICollectionView!
    var activityView : NVActivityIndicatorView!
    var gifs = [String]()
    var selectedGifs = [UIImage]()
    let g = Giphy(apiKey: Giphy.PublicBetaAPIKey)
    var backBtn = UIButton()
    var nextBtn = UIButton()
    var answer = ""
    var didUpdateConstraints = false
    var _counterOfSelectedItems = 0
    var counterOfSelectedItems : Int {
        set {
            if (newValue >= 0 && newValue <= 4) {
                _counterOfSelectedItems = newValue
                if (_counterOfSelectedItems == 4) {
                    UIView.animate(withDuration: 0.25, animations: {
                        self.nextBtn.alpha = 1.0
                    })
                }
                else {
                    UIView.animate(withDuration: 0.25, animations: {
                        self.nextBtn.alpha = 0.0
                    })
                }
            }
        }
        get { return _counterOfSelectedItems}
    }
    var logoLabel = LTMorphingLabel()
    var bottomLogoLabel = LTMorphingLabel()
    var textToSearch = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let gradientView = GPGradientView(frame: self.view.bounds)
        self.view.addSubview(gradientView)
        
        logoLabel = LTMorphingLabel.init(frame: CGRect(x: 50, y: 25, width: SCREEN_WIDTH - 100, height: 50))
        logoLabel.text = "Pick 4 Gifs"
        logoLabel.morphingEffect = .scale
        logoLabel.numberOfLines = 2;
        logoLabel.adjustsFontSizeToFitWidth = true
        logoLabel.textAlignment = .center
        logoLabel.morphingDuration = 1
        logoLabel.font = UIFont(name: "AvenirNext-UltraLight", size: 25)
        logoLabel.textColor = UIColor.white
        self.view.addSubview(logoLabel)
        
        
        bottomLogoLabel = LTMorphingLabel.init(frame: CGRect(x: 50, y: 25, width: SCREEN_WIDTH - 100, height: 30))
        bottomLogoLabel.text = "0/4"
        bottomLogoLabel.numberOfLines = 2;
        bottomLogoLabel.morphingEffect = .burn
        bottomLogoLabel.adjustsFontSizeToFitWidth = true
        bottomLogoLabel.textAlignment = .center
        bottomLogoLabel.clipsToBounds = false
        bottomLogoLabel.morphingDuration = 1
        bottomLogoLabel.font = UIFont(name: "AvenirNext-UltraLight", size: 23)
        bottomLogoLabel.textColor = UIColor.white
        self.view.addSubview(bottomLogoLabel)
        
        
        self.view.bringSubview(toFront: collectionView)
        collectionView.allowsMultipleSelection = true
        
        activityView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width:50, height:50), type: NVActivityIndicatorType.orbit, color: UIColor.white, padding: 0)
        activityView.layer.position = CGPoint(x: SCREEN_WIDTH/2, y: SCREEN_HEIGHT/2)
        self.view.addSubview(activityView)
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        self.backBtn.setImage(UIImage.init(named: "back")?.scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.backBtn.imageView?.contentMode = .scaleAspectFit
        self.backBtn.tintColor = UIColor.white
        self.backBtn.addTarget(self, action: #selector(backBtnPressed), for: .touchUpInside)
        self.view.addSubview(backBtn)
        
        self.nextBtn.setImage(UIImage.init(named: "next")?.scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.nextBtn.imageView?.contentMode = .scaleAspectFit
        self.nextBtn.tintColor = UIColor.white
        self.nextBtn.alpha = 0.0
        self.nextBtn.addTarget(self, action: #selector(nextBtnPressed), for: .touchUpInside)
        self.view.addSubview(nextBtn)
        
        self.updateViewConstraints()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.loadGiphyGifs(text: self.textToSearch)
        self.startDetailsNotification()
        self.counterOfSelectedItems = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.gifs.removeAll()
    }
    
    override func updateViewConstraints() {
        
        if (!didUpdateConstraints) {
            
            self.backBtn.autoPinEdge(toSuperviewEdge: .left, withInset: 15)
            self.backBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))
            self.backBtn.autoAlignAxis(.horizontal, toSameAxisOf: self.logoLabel)
            
            self.nextBtn.autoPinEdge(toSuperviewEdge: .right, withInset: 15)
            self.nextBtn.autoPinEdge(toSuperviewEdge: .bottom, withInset: 25)
            self.nextBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))
            
            self.collectionView.autoPinEdge(toSuperviewEdge: .left)
            self.collectionView.autoPinEdge(toSuperviewEdge: .right)
            self.collectionView.autoPinEdge(.top, to: .bottom, of: self.logoLabel, withOffset: 10)
            self.collectionView.autoPinEdge(.bottom, to: .top, of: self.nextBtn, withOffset: -15)
            
            self.bottomLogoLabel.autoAlignAxis(toSuperviewAxis: .vertical)
            self.bottomLogoLabel.autoSetDimension(.height, toSize: 35)
            self.bottomLogoLabel.autoAlignAxis(.horizontal, toSameAxisOf: self.nextBtn)
            
            self.didUpdateConstraints = true
        }
        
        super.updateViewConstraints()
    }
    
    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func nextBtnPressed() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let controller = storyboard.instantiateViewController(withIdentifier: "PredictVC") as? PredictVC {
            controller.selectedGifs = self.selectedGifs
            controller.answer = self.answer
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func startDetailsNotification() {
        self.logoLabel.text = "Pick 4 GIFs"
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.logoLabel.text = "Hide your screen from your opponent"
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.logoLabel.text = "Tap on arrow to continue"
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.startDetailsNotification()
                }
            }
        }
    }
    
    func loadGiphyGifs(text: String) {
        self.activityView.startAnimating()
        g.search(text, limit: 30, offset: nil, rating: nil) { gifs, pagination, err in
            
            DispatchQueue.main.async {
                self.activityView.stopAnimating()
            }
            
            if let gifArray = gifs {
                
                self.gifs.removeAll()
                
                for var gif: Giphy.Gif in gifArray {
                    self.gifs.append(gif.gifMetadataForType(.FixedWidth, still: false).URL.absoluteString)
                }
                
                DispatchQueue.main.async {
                    self.activityView.stopAnimating()
                    self.collectionView.reloadData()
                    
                }
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    //MARK: - UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.gifs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mozaikCell", for: indexPath) as! GIFCollectionViewCell
        let imageView: UIImageView = cell.gifImageView
        let gifURL = self.gifs[indexPath.row]
        imageView.kf.setImage(with: URL.init(string: gifURL), placeholder: UIImage.gifImageWithName("loading"), options: [.transition(.fade(0.2))], progressBlock: nil, completionHandler: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        //        cell.transform = CGAffineTransform.init(scaleX: 0.9, y: 0.9)
        //        cell.alpha = 0.0
        //
        //        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
        //            cell.transform = CGAffineTransform.init(scaleX: 1, y: 1)
        //            cell.alpha = 1.0
        //        }, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        if (self.counterOfSelectedItems >= 4) {
            return false
        }
        self.counterOfSelectedItems = self.counterOfSelectedItems + 1
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldDeselectItemAt indexPath: IndexPath) -> Bool {
        self.counterOfSelectedItems = self.counterOfSelectedItems - 1
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.bottomLogoLabel.text = "\(self.counterOfSelectedItems)/4"
        let cell = collectionView.cellForItem(at: indexPath) as! GIFCollectionViewCell
        if let image = cell.gifImageView.image {
            self.selectedGifs.append(image)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        self.bottomLogoLabel.text = "\(self.counterOfSelectedItems)/4"
        let cell = collectionView.cellForItem(at: indexPath) as! GIFCollectionViewCell
        if let image = cell.gifImageView.image {
            self.selectedGifs.remove(object: image)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let cellSize:CGSize!
        
        let rand = Int(arc4random_uniform(UInt32(8)));
        
        var width =  SCREEN_WIDTH/2 - 10
        var height =  SCREEN_HEIGHT/6
        
        switch rand {
        case 0:
            width = SCREEN_WIDTH/2 - 10
            height = SCREEN_HEIGHT / 6
        case 1:
            width = SCREEN_WIDTH - 20
            height = SCREEN_HEIGHT / 3
        default :
            width =  SCREEN_WIDTH/2 - 10
            height =  SCREEN_HEIGHT/6
        }
        
        cellSize = CGSize(width: width, height: height)
        
        // return cell size
        return cellSize
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
