//
//  WelcomeScreenVC.swift
//  GIF Predictor
//
//  Created by Altynbek Orumbayev on 29/01/2017.
//  Copyright © 2017 A_Orumbayev. All rights reserved.
//

import UIKit
import PureLayout

class InfoVC: UIViewController {
    var backBtn = UIButton()
    var didUpdateConstraints = false
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    
    override func updateViewConstraints() {
        
        if (!didUpdateConstraints) {
            
            self.backBtn.autoPinEdge(toSuperviewEdge: .top, withInset: 25)
            self.backBtn.autoPinEdge(toSuperviewEdge: .left, withInset: 15)
            self.backBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))
        
            self.didUpdateConstraints = true
        }
        
        super.updateViewConstraints()
    }

    
    // MARK: - Private
    
    func setupViews() {
        // Setting up Gradient View
        
        self.backBtn.setImage(UIImage.init(named: "back")?.scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.backBtn.imageView?.contentMode = .scaleAspectFit
        self.backBtn.tintColor = UIColor.white
        self.backBtn.addTarget(self, action: #selector(backBtnPressed), for: .touchUpInside)
        self.view.addSubview(backBtn)
        
        self.updateViewConstraints()
        
   }
    
    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}
