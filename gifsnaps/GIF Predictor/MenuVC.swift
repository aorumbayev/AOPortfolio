//
//  ViewController.swift
//  GIF Predictor
//
//  Created by Altynbek Orumbayev on 22/01/2017.
//  Copyright © 2017 A_Orumbayev. All rights reserved.
//

import UIKit
import PureLayout
import Gifu

class MenuVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    fileprivate var textArray = [
        "Game rules are super simple!", "Tap on one of the button's below","I'll tell you what to do next", "Let the game begin!"
    ]
    
    var morphingArrayAnimationCounter = 0
    
    var didUpdateConstraints = false
    var cameraButton : GPCircularButton!
    var libraryButton : GPCircularButton!
    var imageToAnalyze = UIImage()
    var logoLabel = LTMorphingLabel()
    var infoBtn = UIButton()
    var questionBtn = UIButton()
    
    var poweredImageView = GIFImageView().configureForAutoLayout()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        startMorphingLabelAnimation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func updateViewConstraints() {
        if (!didUpdateConstraints) {
            
            logoLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 50)
            logoLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 15)
            logoLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 15)
            logoLabel.autoSetDimension(.height, toSize: 50)
            
            poweredImageView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 15)
            poweredImageView.autoPinEdge(toSuperviewEdge: .left, withInset: 10)
            poweredImageView.autoPinEdge(toSuperviewEdge: .right, withInset: 10)
            poweredImageView.autoSetDimension(.height, toSize: 25)
            
            self.questionBtn.autoPinEdge(toSuperviewEdge: .left, withInset: 15)
            self.questionBtn.autoPinEdge(toSuperviewEdge: .bottom, withInset: 15)
            self.questionBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))
            
            
            self.infoBtn.autoPinEdge(toSuperviewEdge: .right, withInset: 15)
            self.infoBtn.autoPinEdge(toSuperviewEdge: .bottom, withInset: 15)
            self.infoBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))
            
            
            self.didUpdateConstraints = true
        }
        
        super.updateViewConstraints()
    }
    
    // MARK: - Private
    
    func setupViews() {
        // Setting up Gradient View
        let gradientView = GPGradientView(frame: self.view.bounds)
        self.view.addSubview(gradientView)
        
        cameraButton = GPCircularButton.init(frame:CGRect.init(x: 0, y: 0, width: CIRCULAR_BTN_SIZE, height: CIRCULAR_BTN_SIZE))
        cameraButton.setImage(UIImage.init(named: "cameraBtnIcon"), for: .normal)
        cameraButton.addTarget(self, action: #selector(self.takePhotoBtnPressed), for: .touchUpInside)
        cameraButton.layer.position = CGPoint.init(x: SCREEN_WIDTH/2, y: SCREEN_HEIGHT/2 - CIRCULAR_BTN_SIZE/2)
        self.view.addSubview(cameraButton)
        
        libraryButton = GPCircularButton.init(frame:CGRect.init(x: 0, y: 0, width: CIRCULAR_BTN_SIZE, height: CIRCULAR_BTN_SIZE))
        libraryButton.setImage(UIImage.init(named: "library"), for: .normal)
        libraryButton.addTarget(self, action: #selector(self.libraryBtnPressed), for: .touchUpInside)
        libraryButton.layer.position = CGPoint.init(x: SCREEN_WIDTH/2, y: cameraButton.layer.position.y + CIRCULAR_BTN_SIZE + 30)
        self.view.addSubview(libraryButton)
        
        logoLabel = LTMorphingLabel.init(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
        logoLabel.text = "Hi there!"
        logoLabel.morphingEffect = .scale
        logoLabel.numberOfLines = 2;
        logoLabel.adjustsFontSizeToFitWidth = true
        logoLabel.textAlignment = .center
        logoLabel.morphingDuration = 1
        logoLabel.font = UIFont(name: "AvenirNext", size: 20)
        logoLabel.textColor = UIColor.white
        
        self.view.addSubview(poweredImageView)
        poweredImageView.animate(withGIFNamed: "giffy")
        poweredImageView.contentMode = .scaleAspectFit
        
        self.view.addSubview(logoLabel)
        
        self.questionBtn.setImage(UIImage.init(named: "question")?.scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.questionBtn.imageView?.contentMode = .scaleAspectFit
        self.questionBtn.tintColor = UIColor.white
        self.questionBtn.addTarget(self, action: #selector(questionBtnPressed), for: .touchUpInside)
        self.view.addSubview(questionBtn)
        
        self.infoBtn.setImage(UIImage.init(named: "info")?.scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.infoBtn.imageView?.contentMode = .scaleAspectFit
        self.infoBtn.tintColor = UIColor.white
        self.infoBtn.addTarget(self, action: #selector(infoBtnPressed), for: .touchUpInside)
        self.view.addSubview(infoBtn)
        
        self.updateViewConstraints()
    }
    
    func startMorphingLabelAnimation() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            
            if let effect = LTMorphingEffect.init(rawValue: self.morphingArrayAnimationCounter) {
                self.logoLabel.morphingEffect = effect
                self.logoLabel.text = self.textArray[self.morphingArrayAnimationCounter]
            }
            
            self.logoLabel.text = self.textArray[self.morphingArrayAnimationCounter]
            
            
            self.morphingArrayAnimationCounter += 1
            
            if (self.morphingArrayAnimationCounter == self.textArray.count) {
                self.morphingArrayAnimationCounter = 0
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.startMorphingLabelAnimation()
            }
        }
    }
    
    @objc func takePhotoBtnPressed() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            let picker = UIImagePickerController.init();
            picker.delegate = self
            picker.allowsEditing = true
            picker.sourceType = .camera
            
            self.present(picker, animated: true, completion: nil)
        }
    }
    
    @objc func libraryBtnPressed() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            
            let picker = UIImagePickerController.init();
            picker.delegate = self
            picker.allowsEditing = true
            picker.sourceType = .photoLibrary
            
            self.present(picker, animated: true, completion: nil)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func infoBtnPressed() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "InfoVC")
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @objc func questionBtnPressed() {
        let vc = TutorialVC()
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let snapshot = appDelegate.window?.snapshotView(afterScreenUpdates: true)
            vc.view.addSubview(snapshot!)
            appDelegate.window?.rootViewController = vc
            
            UIView.animate(withDuration: 0.25, animations: {
                snapshot?.layer.opacity = 0
            }, completion: { (status) in
                snapshot?.removeFromSuperview()
            })
        }
    }
    
    //MARK: - UIImagePicker Delegates
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imageToAnalyze = pickedImage
            
            let newVC = AnalyzerVC()
            
            newVC.imageToAnalyze = self.imageToAnalyze
            
            picker.dismiss(animated: true) {
                self.navigationController?.pushViewController(newVC, animated: true)
            }
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

