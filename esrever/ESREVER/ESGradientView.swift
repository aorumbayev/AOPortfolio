//
//  GPGradientView.swift
//  GIF Predictor
//
//  Created by Altynbek Orumbayev on 29/01/2017.
//  Copyright © 2017 A_Orumbayev. All rights reserved.
//

import UIKit

class GradientColors {
    static let sharedInstance = GradientColors()

    var gradientColors: [CGColor] {
        return [UIColor.init(r: 1, g: 1, b: 1).cgColor,
                UIColor.init(r: 128, g: 1, b: 1).cgColor,
                UIColor.init(r: 135, g: 1, b: 1).cgColor,
                UIColor.init(r: 1, g: 1, b: 1).cgColor,
                UIColor.init(r: 44, g: 1, b: 1).cgColor,
                UIColor.init(r: 35, g: 35, b: 35).cgColor]
 }

    func randomColor() -> [CGColor] {
        let randomIndex1 = Int(arc4random_uniform(UInt32(gradientColors.count)))
        let randomIndex2 = Int(arc4random_uniform(UInt32(gradientColors.count)))

        return [gradientColors[randomIndex1], gradientColors[randomIndex2]]
    }
}

class ESGradientView: UIView, CAAnimationDelegate {

    let gradient = CAGradientLayer()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupGradient()
        animateGradient()
    }

    func setupGradient() {
        gradient.colors = GradientColors.sharedInstance.randomColor()
        gradient.locations = [0.0, 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = UIScreen.main.bounds

        self.layer.insertSublayer(gradient, at: 0)
    }

    func animateGradient() {

        let animation: CABasicAnimation = CABasicAnimation(keyPath: "colors")
        animation.fromValue = gradient.colors
        let randomColor = GradientColors.sharedInstance.randomColor()
        animation.toValue = randomColor
        animation.duration = 5
        animation.isRemovedOnCompletion = true
        animation.fillMode = kCAFillModeBoth
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        animation.delegate = self

        gradient.colors = randomColor
        gradient.add(animation, forKey: "gradientAnimation")
    }

    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag {
            animateGradient()
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupGradient()
        animateGradient()
    }
}
