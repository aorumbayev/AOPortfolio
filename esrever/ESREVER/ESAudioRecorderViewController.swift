//
//  ESAudioRecorderViewController.swift
//  ESREVER
//
//  Created by Altynbek Orumbayev on 6/26/17.
//  Copyright © 2017 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import PureLayout
import SwiftySound
import AVFoundation
import TOMSMorphingLabel
import IQAudioRecorderController

protocol ESAudioRecorderViewControllerDelegate: class {
    func didRecordedAudio(word: String, wordRange: NSRange, player: AVAudioPlayer)
}

class ESAudioRecorderViewController: UIViewController, IQAudioRecorderViewControllerDelegate {

    var player = AVAudioPlayer()
    weak public var delegate: ESAudioRecorderViewControllerDelegate?
    var reversePlayer = AVAudioPlayer()
    var reversedAudioModel: ReversedAudioModel?
    var reversedSecondAudioModel: ReversedAudioModel?
    var hasRecording = false
    var recordingWord: String = "" {
        didSet {
            self.recordingWordLabel.text = recordingWord
            self.setMorphingText(text: recordingWord, nextText: String(recordingWord.reversed()))
        }
    }
    var recordingWordRange: NSRange?

    lazy var backgroundGradientView: ESGradientView! = {
        var tempBackground = ESGradientView()
        return tempBackground
    }()

    lazy var backBtn: UIButton! = {
        var tempBackBtn = UIButton().configureForAutoLayout()
        tempBackBtn.setImage(#imageLiteral(resourceName: "backIcon").scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        tempBackBtn.imageView?.contentMode = .scaleAspectFit
        tempBackBtn.tintColor = UIColor.white
        tempBackBtn.addTarget(self, action: #selector(backBtnPressed), for: .touchUpInside)
        return tempBackBtn
    }()

    lazy var nextBtn: UIButton! = {
        var tempNextBtn = UIButton().configureForAutoLayout()
        tempNextBtn.setImage(#imageLiteral(resourceName: "doneBtn").scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        tempNextBtn.imageView?.contentMode = .scaleAspectFit
        tempNextBtn.tintColor = UIColor.white
        tempNextBtn.alpha = 0.0
        tempNextBtn.addTarget(self, action: #selector(nextBtnPressed), for: .touchUpInside)
        return tempNextBtn
    }()

    lazy var resetBtn: UIButton! = {
        var tempResetBtn = UIButton().configureForAutoLayout()
        tempResetBtn.setImage(#imageLiteral(resourceName: "resetIcon").scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        tempResetBtn.imageView?.contentMode = .scaleAspectFit
        tempResetBtn.tintColor = UIColor.white
        tempResetBtn.alpha = 0.0
        tempResetBtn.tag = 1
        tempResetBtn.addTarget(self, action: #selector(resetBtnPressed), for: .touchUpInside)
        return tempResetBtn
    }()

    lazy var secondResetBtn: UIButton! = {
        var tempSecondResetBtn = UIButton().configureForAutoLayout()
        tempSecondResetBtn.setImage(#imageLiteral(resourceName: "resetIcon").scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        tempSecondResetBtn.imageView?.contentMode = .scaleAspectFit
        tempSecondResetBtn.tintColor = UIColor.white
        tempSecondResetBtn.alpha = 0.0
        tempSecondResetBtn.tag = 2
        tempSecondResetBtn.addTarget(self, action: #selector(resetBtnPressed), for: .touchUpInside)
        return tempSecondResetBtn
    }()

    lazy var thisIsReversedLabel: UILabel! = {
        var tempThisIsReversedLabel = UILabel(frame: CGRect.init(x: 0, y: 0, width: Constants.screenWidth / 1.5, height: 30))
        tempThisIsReversedLabel.textColor = .white
        tempThisIsReversedLabel.textAlignment = .center
        tempThisIsReversedLabel.text = "This is your reversed speech"
        tempThisIsReversedLabel.font = UIFont(name: "AvantGarde-CondBook", size: 18)
        tempThisIsReversedLabel.layer.anchorPoint = CGPoint.init(x: 0.5, y: 0)
        tempThisIsReversedLabel.layer.position = CGPoint.init(x: Constants.screenWidth/2, y: 65)
        tempThisIsReversedLabel.alpha = 0
        tempThisIsReversedLabel.adjustsFontSizeToFitWidth = true
        return tempThisIsReversedLabel
    }()

    lazy var thisIsReverseReversedLabel: TOMSMorphingLabel! = {
        var tempThisIsReverseReversedLabel = TOMSMorphingLabel(frame: CGRect.init(x: 0, y: 0, width: Constants.screenWidth / 1.5, height: 30))
        tempThisIsReverseReversedLabel.textColor = .white
        tempThisIsReverseReversedLabel.textAlignment = .center
        tempThisIsReverseReversedLabel.font = UIFont(name: "AvantGarde-CondBook", size: 18)
        tempThisIsReverseReversedLabel.layer.anchorPoint = CGPoint.init(x: 0.5, y: 0)
        tempThisIsReverseReversedLabel.characterShrinkFactor = 2
        tempThisIsReverseReversedLabel.setTextWithoutMorphing("Repeat your reversed speech")
        tempThisIsReverseReversedLabel.layer.position = CGPoint.init(x: Constants.screenWidth/2, y: 50)
        tempThisIsReverseReversedLabel.alpha = 0
        tempThisIsReverseReversedLabel.adjustsFontSizeToFitWidth = true
        return tempThisIsReverseReversedLabel
    }()

    lazy var recordingWordLabel: TOMSMorphingLabel! = {
        var label = TOMSMorphingLabel().configureForAutoLayout()
        label.textColor = .white
        label.textAlignment = .center
        label.baselineAdjustment = .alignCenters
        label.font = UIFont(name: "AvantGarde-CondBook", size: 26)
        label.characterShrinkFactor = 2
        label.adjustsFontSizeToFitWidth = true
        return label
    }()

    lazy var recordButton: ESPopButton! = {
        var tempRecordButton = ESPopButton.init(frame:CGRect.init(x: 0, y: 0, width: Constants.circularBtnSize, height: Constants.circularBtnSize))
        tempRecordButton.setImage(#imageLiteral(resourceName: "recordButton"), for: .normal)
        tempRecordButton.imageView?.contentMode = .scaleAspectFit
        tempRecordButton.tag = 1
        tempRecordButton.addTarget(self, action: #selector(recordBtnPressed(_:)), for: .touchUpInside)
        tempRecordButton.layer.position = CGPoint.init(x: Constants.screenWidth/2, y: Constants.screenHeight/2)
        return tempRecordButton
    }()

    lazy var reverseRecordButton: ESPopButton! = {
        var tempReverseRecordButton = ESPopButton.init(frame:CGRect.init(x: 0,
                                                                         y: 0,
                                                                         width: Constants.circularBtnSize,
                                                                         height: Constants.circularBtnSize))
        tempReverseRecordButton.setImage(#imageLiteral(resourceName: "recordButton"), for: .normal)
        tempReverseRecordButton.imageView?.contentMode = .scaleAspectFit
        tempReverseRecordButton.tag = 2
        tempReverseRecordButton.alpha = 0
        tempReverseRecordButton.addTarget(self, action: #selector(recordBtnPressed(_:)), for: .touchUpInside)
        tempReverseRecordButton.layer.position = CGPoint.init(x: Constants.screenWidth/2,
                                                              y: self.recordButton.layer.position.y + self.recordButton.frame.size.height/2 + 25)
        return tempReverseRecordButton
    }()

    var didUpdateConstraints = false

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    override func updateViewConstraints() {

        if !didUpdateConstraints {
            self.backgroundGradientView.autoPinEdgesToSuperviewEdges()

            self.backBtn.autoPinEdge(toSuperviewEdge: .left, withInset: 15)
            self.backBtn.autoPinEdge(toSuperviewEdge: .top, withInset: 25)
            self.backBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))

            self.resetBtn.autoPinEdge(toSuperviewEdge: .right, withInset: 15)
            self.resetBtn.autoPinEdge(toSuperviewEdge: .top, withInset: 25)
            self.resetBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))

            self.recordingWordLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 25)
            self.recordingWordLabel.autoPinEdge(.left, to: .right, of: self.backBtn)
            self.recordingWordLabel.autoPinEdge(.right, to: .left, of: self.resetBtn)
            self.recordingWordLabel.autoAlignAxis(.horizontal, toSameAxisOf: self.backBtn)

            self.nextBtn.autoPinEdge(toSuperviewEdge: .right, withInset: 15)
            self.nextBtn.autoPinEdge(toSuperviewEdge: .bottom, withInset: 25)
            self.nextBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))

            self.secondResetBtn.autoPinEdge(toSuperviewEdge: .left, withInset: 15)
            self.secondResetBtn.autoPinEdge(toSuperviewEdge: .bottom, withInset: 25)
            self.secondResetBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))

            self.didUpdateConstraints = true
        }

        super.updateViewConstraints()
    }

    // MARK: - Actions

    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func nextBtnPressed() {
        guard let range = self.recordingWordRange as NSRange? else {return}
        self.delegate?.didRecordedAudio(word: self.recordingWord, wordRange: range, player: self.reversePlayer)
        self.navigationController?.popViewController(animated: true)
    }

    @objc func resetBtnPressed(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            reversedAudioModel = nil
            reversedSecondAudioModel = nil
            animateSecondAudioRecorded()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                self.animateFirstAudioRecorded()
            }
            break

        default:
            reversedSecondAudioModel = nil
            animateSecondAudioRecorded()
            break
        }
    }

    func setupViews() {
        self.view.addSubview(backgroundGradientView)
        self.view.addSubview(recordButton)
        self.view.addSubview(reverseRecordButton)
        self.view.addSubview(nextBtn)
        self.view.addSubview(backBtn)
        self.view.addSubview(resetBtn)
        self.view.addSubview(secondResetBtn)
        self.view.addSubview(thisIsReversedLabel)
        self.view.addSubview(thisIsReverseReversedLabel)
        self.view.addSubview(recordingWordLabel)

        self.updateViewConstraints()
    }

    func sliderValueSnapper(_ sender: UISlider) {

        let curPlayer = sender.tag == 1 ? self.player : self.reversePlayer

        if (sender.value > 0.95 && sender.value < 1.05) {
            sender.value = 1.0
        }

        curPlayer.rate = sender.value
    }

    @objc func recordBtnPressed(_ sender: UIButton) {
        switch sender.tag {
        case 2:
            let hasAudio = reversedSecondAudioModel?.state == .ready && reversedSecondAudioModel != nil
            self.presentRecordViewController(hasRecording: hasAudio, label: hasAudio ? "" : "Repeat reversed speach", player: self.reversePlayer)
            break
        default:
            let hasAudio = reversedAudioModel?.state == .ready && reversedAudioModel != nil
            self.presentRecordViewController(hasRecording: hasAudio, label: hasAudio ? "" : "Say something meaningful", player: self.player)
            break
        }
    }

    func presentRecordViewController(hasRecording: Bool, label: String, player: AVAudioPlayer) {
        if (!hasRecording) {
            let recorderVC = IQAudioRecorderViewController()
            recorderVC.title = label
            recorderVC.allowCropping = true
            recorderVC.barStyle = .black
            recorderVC.delegate = self
            recorderVC.normalTintColor = .white
            recorderVC.highlightedTintColor = .red
            self.presentBlurredAudioRecorderViewControllerAnimated(recorderVC)
        } else {
            Sound.play(url: player.url!)
        }
    }

    func animateFirstAudioRecorded() {
        let hasRecording = self.reversedAudioModel?.state == .ready
     
        UIView.animate(withDuration: 0.25, delay: 0.0, options: [.curveEaseInOut], animations: {
            self.recordButton.layer.position = CGPoint.init(x: Constants.screenWidth/2,
                                                            y: (hasRecording ? self.thisIsReversedLabel.posY + 30 + self.recordButton.frame.width / 2 + 20
                                                                : Constants.screenHeight/2))
            self.thisIsReverseReversedLabel.layer.position = CGPoint.init(x: Constants.screenWidth/2,
                                                                          y: self.recordButton.posY + self.recordButton.frame.height + 35)
            self.reverseRecordButton.layer.position = CGPoint.init(x: Constants.screenWidth/2,
                                                                   y: self.thisIsReverseReversedLabel.posY + 30 + self.recordButton.frame.width / 2 + 25)
        }) { (status) in
            if status {
                UIView.animate(withDuration: 0.15, delay: 0.0, options: [.curveEaseInOut], animations: {
                    let alphaFloat = hasRecording ? CGFloat(1.0) : CGFloat(0.0)
                    self.resetBtn.alpha = alphaFloat
                    self.reverseRecordButton.alpha = alphaFloat
                    self.thisIsReversedLabel.alpha = alphaFloat
                    self.thisIsReverseReversedLabel.alpha = alphaFloat
                    self.recordButton.transform = CGAffineTransform.init(scaleX: 0.9, y: 0.9)
                }) { _ in
                    UIView.animate(withDuration: 0.15, delay: 0.0, options: [.curveEaseInOut], animations: {
                        self.recordButton.setImage(hasRecording ? #imageLiteral(resourceName: "playButton") : #imageLiteral(resourceName: "recordButton"), for: .normal)
                        self.recordButton.transform = CGAffineTransform.init(scaleX: 1, y: 1)
                        self.recordButton.alpha = 1.0
                    })
                }
            }
        }
    }

    func animateSecondAudioRecorded() {
        let hasRecording = self.reversedSecondAudioModel?.state == .ready

        UIView.animate(withDuration: 0.25, delay: 0.0, options: [.curveEaseInOut], animations: {
            let alphaFloat = hasRecording ? CGFloat(1.0) : CGFloat(0.0)
            self.reverseRecordButton.alpha = alphaFloat
            self.nextBtn.alpha = alphaFloat
            self.secondResetBtn.alpha = alphaFloat
            self.reverseRecordButton.transform = CGAffineTransform.init(scaleX: 0.9, y: 0.9)
        }) { _ in
            self.thisIsReverseReversedLabel.text = hasRecording ? "This is your esrevered speech" : "Repeat your reversed speech"

            UIView.animate(withDuration: 0.25, delay: 0.0, options: [.curveEaseInOut], animations: {
                self.reverseRecordButton.setImage(hasRecording ? #imageLiteral(resourceName: "reversedPlayButton") : #imageLiteral(resourceName: "recordButton"), for: .normal)
                self.reverseRecordButton.transform = CGAffineTransform.init(scaleX: 1, y: 1)
                self.reverseRecordButton.alpha = 1.0
            })
        }
    }

    func setMorphingText(text: String!, nextText: String!) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5) {
            self.recordingWordLabel.setText(text) {
                self.setMorphingText(text: nextText, nextText: text)
            }
        }
    }

    // MARK: - IQAudioRecorderViewControllerDelegate

    func audioRecorderController(_ controller: IQAudioRecorderViewController, didFinishWithAudioAtPath filePath: String) {
        controller.dismiss(animated: true) {
            let audioURL = URL(fileURLWithPath: filePath)
            (controller.title?.contains("meaningful"))! ? self.reverseAndSetupPlayer(URL: audioURL) : self.reverseAndSetupSecondPlayer(URL: audioURL)
        }
    }

    func reverseAndSetupPlayer(URL: URL) {
        self.reversedAudioModel = ReversedAudioModel(source: URL)
        self.reversedAudioModel?.onStateChange = { [weak self] state in
            guard let strongSelf = self else { return }
            DispatchQueue.main.async {
                do {
                    if state == .ready {
                        guard let backwardLink = strongSelf.reversedAudioModel?.backwardURL else {return}
                        strongSelf.player = try AVAudioPlayer(contentsOf: backwardLink)
                        strongSelf.player.enableRate = true
                        strongSelf.animateFirstAudioRecorded()
                    } else {
                        strongSelf.animateFirstAudioRecorded()
                        return
                    }
                } catch {
                    strongSelf.animateFirstAudioRecorded()
                    return
                }
            }
        }
    }

    func reverseAndSetupSecondPlayer(URL: URL) {
        self.reversedSecondAudioModel = ReversedAudioModel(source: URL)
        self.reversedSecondAudioModel?.onStateChange = { [weak self] state in
            guard let strongSelf = self else { return }
            DispatchQueue.main.async {
                do {
                    if state == .ready {
                        guard let backwardLink = strongSelf.reversedSecondAudioModel?.backwardURL else {return}
                        strongSelf.reversePlayer = try AVAudioPlayer(contentsOf: backwardLink)
                        strongSelf.reversePlayer.enableRate = true
                        strongSelf.animateSecondAudioRecorded()
                    } else {
                        strongSelf.animateSecondAudioRecorded()
                        return
                    }
                } catch {
                    strongSelf.animateSecondAudioRecorded()
                    return
                }
            }
        }
    }

}
