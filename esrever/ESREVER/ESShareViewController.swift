//
//  ESShareViewController.swift
//  ESREVER
//
//  Created by Altynbek Orumbayev on 7/11/17.
//  Copyright © 2017 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import PureLayout
import SwiftySound
import TOMSMorphingLabel

class ESShareViewController: UIViewController {
    
    var backgroundGradientView: ESGradientView!
    var shareButton: ESPopButton!
    var playBtn: ESPopButton!
    var convertedAudioPath: URL?
    var audioPath: URL? {
        didSet {
            guard let path = audioPath as URL? else {return}
            let converter = ExtAudioConverter()
            converter.inputFile = path.path
            converter.outputFile = path.path.replacingOccurrences(of: path.lastPathComponent,
                                                                       with: "esrever\(Date().toString(format: "HH:mm:ss-d:M:yy")).mp3")
            converter.outputFormatID = kAudioFormatMPEGLayer3
            converter.outputFileType = kAudioFileMP3Type
            converter.convert()
            convertedAudioPath = URL(fileURLWithPath: converter.outputFile)
        }
    }
    
    var didUpdateConstraints = false
    var wowLabel: TOMSMorphingLabel! = {
        var tempWowLabel = TOMSMorphingLabel(frame: CGRect.init(x: 0, y: 0, width: Constants.screenWidth / 1.5, height: 100))
        tempWowLabel.textColor = .white
        tempWowLabel.numberOfLines = 0
        tempWowLabel.textAlignment = .center
        tempWowLabel.layer.anchorPoint = CGPoint.init(x: 0.5, y: 0)
        tempWowLabel.characterShrinkFactor = 2
        tempWowLabel.font = UIFont(name: "AvantGarde-CondBook", size: 18)
        tempWowLabel.setTextWithoutMorphing("Wow, Dude, wow.\nNow share your esrevered audio with friends!".uppercased())
        tempWowLabel.sizeToFit()
        return tempWowLabel
    }()
    
    lazy var backBtn: UIButton! = {
        var tempBackBtn = UIButton().configureForAutoLayout()
        tempBackBtn.setImage(#imageLiteral(resourceName: "backIcon").scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        tempBackBtn.imageView?.contentMode = .scaleAspectFit
        tempBackBtn.tintColor = UIColor.white
        tempBackBtn.addTarget(self, action: #selector(backBtnPressed), for: .touchUpInside)
        return tempBackBtn
    }()
    
    lazy var doneBtn: UIButton! = {
        var tempDoneBtn = UIButton().configureForAutoLayout()
        tempDoneBtn.setImage(#imageLiteral(resourceName: "doneBtn").scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        tempDoneBtn.imageView?.contentMode = .scaleAspectFit
        tempDoneBtn.tintColor = UIColor.white
        tempDoneBtn.addTarget(self, action: #selector(doneBtnPressed), for: .touchUpInside)
        return tempDoneBtn
    }()
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setMorphingText(text: "Wow, Dude, wow.\nNow share your esrevered audio with friends!".uppercased(),
                             nextText: "Wow, Dude, wow.\n!sdneirf htiw oidua dereverse ruoy erahs woN".uppercased())
    }
    
    override func updateViewConstraints() {
        
        if !didUpdateConstraints {
            
            self.backBtn.autoPinEdge(toSuperviewEdge: .left, withInset: 15)
            self.backBtn.autoPinEdge(toSuperviewEdge: .top, withInset: 25)
            self.backBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))
            
            self.doneBtn.autoPinEdge(toSuperviewEdge: .right, withInset: 15)
            self.doneBtn.autoPinEdge(toSuperviewEdge: .top, withInset: 25)
            self.doneBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))
            
            self.didUpdateConstraints = true
        }
        
        super.updateViewConstraints()
    }
    
    // MARK: - Actions
    func setupViews() {
        self.backgroundGradientView = ESGradientView(frame: CGRect.init(x: 0, y: 0, width: Constants.screenWidth, height: Constants.screenHeight))
        self.view.addSubview(self.backgroundGradientView)
        
        playBtn = ESPopButton.init(frame:CGRect.init(x: 0, y: 0, width: Constants.circularBtnSize, height: Constants.circularBtnSize))
        playBtn.setImage(#imageLiteral(resourceName: "playButton"), for: .normal)
        playBtn.imageView?.contentMode = .scaleAspectFit
        playBtn.addTarget(self, action: #selector(playBtnPressed), for: .touchUpInside)
        playBtn.layer.position = CGPoint.init(x: Constants.screenWidth/2, y: Constants.screenHeight/2 - Constants.circularBtnSize/2 - 10)
        self.view.addSubview(playBtn)
        
        shareButton = ESPopButton.init(frame:CGRect.init(x: 0, y: 0, width: Constants.circularBtnSize, height: Constants.circularBtnSize))
        shareButton.setImage(#imageLiteral(resourceName: "shareButton"), for: .normal)
        shareButton.imageView?.contentMode = .scaleAspectFit
        shareButton.addTarget(self, action: #selector(shareBtnPressed(_:)), for: .touchUpInside)
        shareButton.layer.position = CGPoint.init(x: Constants.screenWidth/2, y: Constants.screenHeight/2 + Constants.circularBtnSize/2 + 10)
        self.view.addSubview(shareButton)
        
        self.wowLabel.layer.position = CGPoint.init(x: Constants.screenWidth/2,
                                                    y: self.shareButton.layer.position.y + self.shareButton.frame.size.height/2 + 20)
        
        self.view.addSubview(wowLabel)
        self.view.addSubview(backBtn)
        self.view.addSubview(doneBtn)
        
        self.updateViewConstraints()
    }
    
    func setMorphingText(text: String!, nextText: String!) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 4) {
            self.wowLabel.setText(text) {
                self.wowLabel.sizeToFit()
                self.wowLabel.layer.position = CGPoint.init(x: Constants.screenWidth/2,
                                                            y: self.shareButton.layer.position.y + self.shareButton.frame.size.height/2 + 20)
                self.setMorphingText(text: nextText, nextText: text)
            }
        }
    }
    
    @objc func shareBtnPressed(_ sender: UIButton!) {
        
        if let activityItem = self.convertedAudioPath {
            let activityVC = UIActivityViewController(activityItems: [activityItem], applicationActivities: nil)
            activityVC.popoverPresentationController?.sourceView = self.view
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func doneBtnPressed() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func playBtnPressed() {
        if let path = convertedAudioPath as URL? {
            Sound.play(url: path)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
