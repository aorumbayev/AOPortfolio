//
//  Extensions.swift
//  ESREVER
//
//  Created by Altynbek Orumbayev on 7/11/17.
//  Copyright © 2017 Altynbek Orumbayev. All rights reserved.
//

import Foundation
import UIKit
import RazzleDazzle
import AVFoundation

extension Date {
    public func toString(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}

extension String {
    func rangeOfNthWord(wordNum: Int, wordSeparator: String) -> NSRange? {
        let arr = self.components(separatedBy: wordSeparator)
        if arr.count < wordNum {
            return nil
        } else {
            let fromIndex = arr[0..<wordNum].map { $0.characters.count }.reduce(0, +) + (wordNum)*wordSeparator.characters.count
            let length = arr[wordNum].characters.count
            return NSRange.init(location: fromIndex, length: length)
        }
    }
}

public extension NSRange {
    func stringValue() -> String {
        return "length:\(self.length) location:\(self.location)"
    }
}

public extension UIImage {
    
    func scaleImage(toSize newSize: CGSize) -> UIImage? {
        let newRect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height).integral
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        if let context = UIGraphicsGetCurrentContext() {
            context.interpolationQuality = .high
            let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
            context.concatenate(flipVertical)
            context.draw(self.cgImage!, in: newRect)
            let newImage = UIImage(cgImage: context.makeImage()!)
            UIGraphicsEndImageContext()
            return newImage
        }
        return nil
    }
}

private let minimumHitArea = CGSize(width: 30, height: 30)

extension UIButton {
    open override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        // if the button is hidden/disabled/transparent it can't be hit
        if self.isHidden || !self.isUserInteractionEnabled || self.alpha < 0.01 { return nil }
        
        // increase the hit frame to be at least as big as `minimumHitArea`
        let buttonSize = self.bounds.size
        let widthToAdd = max(minimumHitArea.width - buttonSize.width, 0)
        let heightToAdd = max(minimumHitArea.height - buttonSize.height, 0)
        let largerFrame = self.bounds.insetBy(dx: -widthToAdd / 2, dy: -heightToAdd / 2)
        
        // perform hit test on larger frame
        return (largerFrame.contains(point)) ? self : nil
    }
}

extension UIColor {
    /// EZSE: init method with RGB values from 0 to 255, instead of 0 to 1. With alpha(default:1)
    public convenience init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat = 1) {
        self.init(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: a)
    }
}

private let UIViewAnimationDuration: TimeInterval = 1
private let UIViewAnimationSpringDamping: CGFloat = 0.5
private let UIViewAnimationSpringVelocity: CGFloat = 0.5

extension UIView {
    class func loadFromNibNamed(nibNamed: String, bundle: Bundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
    
    public func spring(animations: @escaping (() -> Void), completion: ((Bool) -> Void)? = nil) {
        spring(duration: UIViewAnimationDuration, animations: animations, completion: completion)
    }
    
    public func setScale(x: CGFloat, y: CGFloat) {
        var transform = CATransform3DIdentity
        transform.m34 = 1.0 / -1000.0
        transform = CATransform3DScale(transform, x, y, 1)
        self.layer.transform = transform
    }
    
    public func spring(duration: TimeInterval, animations: @escaping (() -> Void), completion: ((Bool) -> Void)? = nil) {
        UIView.animate(
            withDuration: UIViewAnimationDuration,
            delay: 0,
            usingSpringWithDamping: UIViewAnimationSpringDamping,
            initialSpringVelocity: UIViewAnimationSpringVelocity,
            options: UIViewAnimationOptions.allowAnimatedContent,
            animations: animations,
            completion: completion
        )
    }
    
    public func pop() {
        setScale(x: 1.1, y: 1.1)
        spring(duration: 0.2, animations: { [unowned self] () -> Void in
            self.setScale(x: 1, y: 1)
        })
    }
    
    func startPopAnimation(withDuration duration: Int) {
        self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut, .allowUserInteraction], animations: {
            self.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        }) { (_) in
            
            UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut, .allowUserInteraction], animations: {
                self.transform = CGAffineTransform.identity
            }, completion: nil)
        }
    }
    
    public var posX: CGFloat {
        get {
            return self.layer.position.x
        } set(value) {
            self.layer.position.x = value
        }
    }
    
    public var posY: CGFloat {
        get {
            return self.layer.position.y
        } set(value) {
            self.layer.position.y = value
        }
    }
    
    public var anchorPos: CGPoint {
        get {
            return self.layer.anchorPoint
        } set(value) {
            self.layer.anchorPoint = value
        }
    }
    
    public var pos: CGPoint {
        get {
            return self.layer.position
        } set(value) {
            self.layer.position = value
        }
    }
}

extension UITextView {
    
    func centerVertically() {
        var topCorrect = (self.bounds.size.height - self.contentSize.height * self.zoomScale)/2.0
        topCorrect = (topCorrect < 0.0 ? 0.0 : topCorrect)
        self.contentOffset = CGPoint.init(x: 0, y: -topCorrect)
    }
    
}

extension ESTutorialViewController {
    func configureFirstPage() {
        // Center the star on the page, and keep it centered on pages 0 and 1
        
        let top = NSLayoutConstraint(item: self.page1Text,
                                     attribute: .top,
                                     relatedBy: .equal,
                                     toItem: self.scrollView,
                                     attribute: .top,
                                     multiplier: 1.0,
                                     constant: 25.0)
        
        let height = NSLayoutConstraint(item: self.page1Text,
                                        attribute: .height,
                                        relatedBy: .equal,
                                        toItem: nil,
                                        attribute: .notAnAttribute,
                                        multiplier: 0.0,
                                        constant: 55.0)
        
        let width = NSLayoutConstraint(item: self.page1Text,
                                       attribute: .width,
                                       relatedBy: .equal,
                                       toItem: nil,
                                       attribute: .notAnAttribute,
                                       multiplier: 0.0,
                                       constant: UIScreen.main.bounds.width - 40)
        
        NSLayoutConstraint.activate([width, height, top])
        
        keepView(page1Text, onPage: 0)
        
        let topIphoneIcon = NSLayoutConstraint(item: self.page1Logo,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: self.page1Text,
                                               attribute: .bottom,
                                               multiplier: 1.0,
                                               constant: 25.0)
        
        let leftIphoneIcon = NSLayoutConstraint(item: self.page1Logo,
                                                attribute: .left,
                                                relatedBy: .equal,
                                                toItem: self.page1Text,
                                                attribute: .left,
                                                multiplier: 1.0,
                                                constant: -20.0)
        
        let rightIphoneIcon = NSLayoutConstraint(item: self.page1Logo,
                                                 attribute: .right,
                                                 relatedBy: .equal,
                                                 toItem: self.page1Text,
                                                 attribute: .right,
                                                 multiplier: 1.0,
                                                 constant: 20.0)
        
        let bottomIphoneIcon = NSLayoutConstraint(item: self.page1Logo,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: scrollView,
                                                  attribute: .bottom,
                                                  multiplier: 1.0,
                                                  constant: -100.0)
        
        NSLayoutConstraint.activate([topIphoneIcon, leftIphoneIcon, rightIphoneIcon, bottomIphoneIcon])
        keepView(page1Logo, onPage: 0)
        
        let topIphoneIconLabel = NSLayoutConstraint(item: self.page1TextLabel,
                                                    attribute: .top,
                                                    relatedBy: .equal,
                                                    toItem: self.page1Logo,
                                                    attribute: .bottom,
                                                    multiplier: 1.0,
                                                    constant: 5.0)
        
        let leftIphoneIconLabel = NSLayoutConstraint(item: self.page1TextLabel,
                                                     attribute: .left,
                                                     relatedBy: .equal,
                                                     toItem: self.page1Text,
                                                     attribute: .left,
                                                     multiplier: 1.0,
                                                     constant: 0.0)
        
        let rightIphoneIconLabel = NSLayoutConstraint(item: self.page1TextLabel,
                                                      attribute: .right,
                                                      relatedBy: .equal,
                                                      toItem: self.page1Text,
                                                      attribute: .right,
                                                      multiplier: 1.0,
                                                      constant: 0.0)
        
        let bottomIphoneIconLabel = NSLayoutConstraint(item: self.page1TextLabel,
                                                       attribute: .bottom,
                                                       relatedBy: .equal,
                                                       toItem: scrollView,
                                                       attribute: .bottom,
                                                       multiplier: 1.0,
                                                       constant: 5.0)
        
        NSLayoutConstraint.activate([topIphoneIconLabel, bottomIphoneIconLabel, leftIphoneIconLabel, rightIphoneIconLabel])
        keepView(page1TextLabel, onPage: 0)
        
        let alphaAnimationLogo = AlphaAnimation.init(view: self.page1Text)
        alphaAnimationLogo[0] = 1
        alphaAnimationLogo[0.1] = 0
        alphaAnimationLogo[1] = 0
        animator.addAnimation(alphaAnimationLogo)
        
        let alphaAnimationLogoLabel = AlphaAnimation.init(view: self.page1TextLabel)
        alphaAnimationLogoLabel[0] = 1
        alphaAnimationLogoLabel[0.1] = 0
        alphaAnimationLogoLabel[1] = 0
        animator.addAnimation(alphaAnimationLogoLabel)
        
        let scaleImageAnimation = ScaleAnimation.init(view: page1Logo)
        scaleImageAnimation.addKeyframe(0, value: 1, easing: EasingFunctionEaseInQuad)
        scaleImageAnimation[1] = 5
        animator.addAnimation(scaleImageAnimation)
        
        let leftConstantAnimation = ConstraintConstantAnimation.init(superview: scrollView, constraint: leftIphoneIcon)
        leftConstantAnimation[0] = -20
        leftConstantAnimation[0.75] = 400
        animator.addAnimation(leftConstantAnimation)
        
        let rightConstantAnimation = ConstraintConstantAnimation.init(superview: scrollView, constraint: rightIphoneIcon)
        rightConstantAnimation[0] = 20
        rightConstantAnimation[0.5] = 400
        animator.addAnimation(rightConstantAnimation)
        
        let alphaAnimationIcon = AlphaAnimation.init(view: self.page1Logo)
        alphaAnimationIcon[0] = 1
        alphaAnimationIcon[0.6] = 0
        animator.addAnimation(alphaAnimationIcon)
    }
    
    func configureSecondPage() {
        
        NSLayoutConstraint(item: page2Text,
                           attribute: .top,
                           relatedBy: .equal,
                           toItem: self.contentView,
                           attribute: .top,
                           multiplier: 1.0,
                           constant: 25.0).isActive = true
        
        //          NSLayoutConstraint(item: page2Text,
        //                                         attribute: .centerX,
        //                                         relatedBy: .equal,
        //                                         toItem: self.scrollView,
        //                                         attribute: .centerX,
        //                                         multiplier: 1.0,
        //                                         constant: 0).isActive = true
        //
        NSLayoutConstraint(item: page2Text,
                           attribute: .height,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: 65.0).isActive = true
        
        NSLayoutConstraint(item: page2Text,
                           attribute: .width,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: UIScreen.main.bounds.width - 40).isActive = true
        
        keepView(page2Text, onPage:1)
        
        let topIphoneIcon = NSLayoutConstraint(item: page2Logo,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: self.page2Text,
                                               attribute: .bottom,
                                               multiplier: 1.0,
                                               constant: 25.0)
        
        let leftIphoneIcon = NSLayoutConstraint(item: page2Logo,
                                                attribute: .left,
                                                relatedBy: .equal,
                                                toItem: self.page2Text,
                                                attribute: .left,
                                                multiplier: 1.0,
                                                constant: -20.0)
        
        let rightIphoneIcon = NSLayoutConstraint(item: page2Logo,
                                                 attribute: .right,
                                                 relatedBy: .equal,
                                                 toItem: self.page2Text,
                                                 attribute: .right,
                                                 multiplier: 1.0,
                                                 constant: 20.0)
        
        let bottomIphoneIcon = NSLayoutConstraint(item: page2Logo,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: scrollView,
                                                  attribute: .bottom,
                                                  multiplier: 1.0,
                                                  constant: -100.0)
        
        NSLayoutConstraint.activate([topIphoneIcon, leftIphoneIcon, rightIphoneIcon, bottomIphoneIcon])
        keepView(page2Logo, onPage:1)
        
        let alphaAnimationLogo = AlphaAnimation.init(view: self.page2Text)
        alphaAnimationLogo[1] = 1
        alphaAnimationLogo[1.1] = 0
        alphaAnimationLogo[2] = 0
        animator.addAnimation(alphaAnimationLogo)
        
        let scaleImageAnimation = ScaleAnimation.init(view: page2Logo)
        scaleImageAnimation.addKeyframe(0, value: 0, easing: EasingFunctionEaseInQuad)
        scaleImageAnimation[0.7] = 0.9
        scaleImageAnimation[0.85] = 1.1
        scaleImageAnimation[1] = 1
        scaleImageAnimation[1.15] = 1.1
        scaleImageAnimation[1.3] = 0.9
        animator.addAnimation(scaleImageAnimation)
        
        let leftConstantAnimation = ConstraintConstantAnimation.init(superview: scrollView, constraint: leftIphoneIcon)
        leftConstantAnimation[1] = -20
        leftConstantAnimation[1.75] = -400
        animator.addAnimation(leftConstantAnimation)
        
    }
    
    func configureThirdPage() {
        
        NSLayoutConstraint(item: page3Text,
                           attribute: .top,
                           relatedBy: .equal,
                           toItem: self.contentView,
                           attribute: .top,
                           multiplier: 1.0,
                           constant: 25.0).isActive = true
        
        NSLayoutConstraint(item: page3Text,
                           attribute: .height,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: 65.0).isActive = true
        
        NSLayoutConstraint(item: page3Text,
                           attribute: .width,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: UIScreen.main.bounds.width - 40).isActive = true
        
        keepView(page3Text, onPage:2)
        
        let topIphoneIcon = NSLayoutConstraint(item: page3Logo,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: self.page3Text,
                                               attribute: .bottom,
                                               multiplier: 1.0,
                                               constant: 25.0)
        
        let leftIphoneIcon = NSLayoutConstraint(item: page3Logo,
                                                attribute: .left,
                                                relatedBy: .equal,
                                                toItem: self.page3Text,
                                                attribute: .left,
                                                multiplier: 1.0,
                                                constant: -20.0)
        
        let rightIphoneIcon = NSLayoutConstraint(item: page3Logo,
                                                 attribute: .right,
                                                 relatedBy: .equal,
                                                 toItem: self.page3Text,
                                                 attribute: .right,
                                                 multiplier: 1.0,
                                                 constant: 20.0)
        
        let bottomIphoneIcon = NSLayoutConstraint(item: page3Logo,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: scrollView,
                                                  attribute: .bottom,
                                                  multiplier: 1.0,
                                                  constant: -100.0)
        
        NSLayoutConstraint.activate([topIphoneIcon, leftIphoneIcon, rightIphoneIcon, bottomIphoneIcon])
        keepView(page3Logo, onPage:2)
        
        let scaleImageAnimation = ScaleAnimation.init(view: page3Logo)
        scaleImageAnimation.addKeyframe(0, value: 0, easing: EasingFunctionEaseInQuad)
        scaleImageAnimation[1.7] = 0.9
        scaleImageAnimation[1.85] = 1.1
        scaleImageAnimation[2] = 1
        scaleImageAnimation[2.15] = 1.1
        scaleImageAnimation[2.3] = 0.9
        animator.addAnimation(scaleImageAnimation)
    }
    
    func configureFourthPage() {
        
        NSLayoutConstraint(item: page4Text,
                           attribute: .top,
                           relatedBy: .equal,
                           toItem: self.contentView,
                           attribute: .top,
                           multiplier: 1.0,
                           constant: 25.0).isActive = true
        
        NSLayoutConstraint(item: page4Text,
                           attribute: .height,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: 65.0).isActive = true
        
        NSLayoutConstraint(item: page4Text,
                           attribute: .width,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: UIScreen.main.bounds.width - 40).isActive = true
        
        keepView(page4Text, onPage:3)
        
        let topIphoneIcon = NSLayoutConstraint(item: page4Logo,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: self.page4Text,
                                               attribute: .bottom,
                                               multiplier: 1.0,
                                               constant: 25.0)
        
        let leftIphoneIcon = NSLayoutConstraint(item: page4Logo,
                                                attribute: .left,
                                                relatedBy: .equal,
                                                toItem: self.page4Text,
                                                attribute: .left,
                                                multiplier: 1.0,
                                                constant: -20.0)
        
        let rightIphoneIcon = NSLayoutConstraint(item: page4Logo,
                                                 attribute: .right,
                                                 relatedBy: .equal,
                                                 toItem: self.page4Text,
                                                 attribute: .right,
                                                 multiplier: 1.0,
                                                 constant: 20.0)
        
        let bottomIphoneIcon = NSLayoutConstraint(item: page4Logo,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: scrollView,
                                                  attribute: .bottom,
                                                  multiplier: 1.0,
                                                  constant: -100.0)
        
        NSLayoutConstraint.activate([topIphoneIcon, leftIphoneIcon, rightIphoneIcon, bottomIphoneIcon])
        keepView(page4Logo, onPage:3)
        
        let scaleImageAnimation = ScaleAnimation.init(view: page4Logo)
        scaleImageAnimation.addKeyframe(0, value: 0, easing: EasingFunctionEaseInQuad)
        scaleImageAnimation[2.7] = 0.9
        scaleImageAnimation[2.85] = 1.1
        scaleImageAnimation[3] = 1
        scaleImageAnimation[3.15] = 1.1
        scaleImageAnimation[3.3] = 0.9
        animator.addAnimation(scaleImageAnimation)
        
    }
    
    func configureFifthPage() {
        
        NSLayoutConstraint(item: page5Text,
                           attribute: .top,
                           relatedBy: .equal,
                           toItem: self.contentView,
                           attribute: .top,
                           multiplier: 1.0,
                           constant: 25.0).isActive = true
        
        NSLayoutConstraint(item: page5Text,
                           attribute: .height,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: 65.0).isActive = true
        
        NSLayoutConstraint(item: page5Text,
                           attribute: .width,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: UIScreen.main.bounds.width - 40).isActive = true
        
        keepView(page5Text, onPage:4)
        
        let topIphoneIcon = NSLayoutConstraint(item: page5Logo,
                                               attribute: .top,
                                               relatedBy: .equal,
                                               toItem: self.page5Text,
                                               attribute: .bottom,
                                               multiplier: 1.0,
                                               constant: 25.0)
        
        let leftIphoneIcon = NSLayoutConstraint(item: page5Logo,
                                                attribute: .left,
                                                relatedBy: .equal,
                                                toItem: self.page5Text,
                                                attribute: .left,
                                                multiplier: 1.0,
                                                constant: -20.0)
        
        let rightIphoneIcon = NSLayoutConstraint(item: page5Logo,
                                                 attribute: .right,
                                                 relatedBy: .equal,
                                                 toItem: self.page5Text,
                                                 attribute: .right,
                                                 multiplier: 1.0,
                                                 constant: 20.0)
        
        let bottomIphoneIcon = NSLayoutConstraint(item: page5Logo,
                                                  attribute: .bottom,
                                                  relatedBy: .equal,
                                                  toItem: scrollView,
                                                  attribute: .bottom,
                                                  multiplier: 1.0,
                                                  constant: -100.0)
        
        NSLayoutConstraint.activate([topIphoneIcon, leftIphoneIcon, rightIphoneIcon, bottomIphoneIcon])
        keepView(page5Logo, onPage:4)
        
        let scaleImageAnimation = ScaleAnimation.init(view: page5Logo)
        scaleImageAnimation.addKeyframe(0, value: 0, easing: EasingFunctionEaseInQuad)
        scaleImageAnimation[3.7] = 0.9
        scaleImageAnimation[3.85] = 1.1
        scaleImageAnimation[4] = 1
        scaleImageAnimation[4.15] = 1.1
        scaleImageAnimation[4.3] = 0.9
        animator.addAnimation(scaleImageAnimation)
        
        NSLayoutConstraint(item: loginBtn,
                           attribute: .width,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: UIScreen.main.bounds.width - 60).isActive = true
        
        NSLayoutConstraint(item: loginBtn,
                           attribute: .height,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 0.0,
                           constant: 45).isActive = true
        
        let bottomLogin = NSLayoutConstraint(item: loginBtn,
                                             attribute: .bottom,
                                             relatedBy: .equal,
                                             toItem: scrollView,
                                             attribute: .bottom,
                                             multiplier: 1.0,
                                             constant: -20)
        
        NSLayoutConstraint.activate([bottomLogin])
        keepView(loginBtn, onPage: 4, withAttribute: .centerX)
    }
}

extension UIViewController {
    
    func mergeAudioFiles(audioFileUrls: NSArray, interval: Double, completion: @escaping(URL?) -> Void) {
        var mergeAudioURL: URL
        let composition = AVMutableComposition()
        
        for i in 0 ..< audioFileUrls.count {
            
            let compositionAudioTrack: AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio,
                                                                                               preferredTrackID: CMPersistentTrackID())!
            
            guard let curURL = audioFileUrls[i] as? NSURL else {return}
            let asset = AVURLAsset(url: (curURL) as URL)
            
            let track = asset.tracks(withMediaType: AVMediaType.audio)[0]
            
            do {
                let timeRange = CMTimeRange(start: CMTimeMake(0, 600), duration: track.timeRange.duration)
                try compositionAudioTrack.insertTimeRange(timeRange, of: track, at: composition.duration)
            } catch {
                return
            }
            
            if (audioFileUrls.count > 1 && interval > 0) {
                let duration = CMTime.init(seconds: interval, preferredTimescale: 600)
                let emptyTimeRange = CMTimeRange(start: CMTimeMake(0, 600), duration: duration)
                compositionAudioTrack.insertEmptyTimeRange(emptyTimeRange)
            }
        }
        
        let documentDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
        mergeAudioURL = documentDirectoryURL.appendingPathComponent("Merge_Audio.m4a")!
        
        let assetExport = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetAppleM4A)
        assetExport?.outputFileType = AVFileType.m4a
        assetExport?.outputURL = mergeAudioURL as URL
        removeFileAtURLIfExists(url: mergeAudioURL as NSURL)
        assetExport?.exportAsynchronously(completionHandler: {
            switch assetExport!.status {
            case AVAssetExportSessionStatus.failed:
                print("failed \(String(describing: assetExport?.error))")
                completion(nil)
            case AVAssetExportSessionStatus.cancelled:
                print("cancelled \(String(describing: assetExport?.error))")
                completion(nil)
            case AVAssetExportSessionStatus.unknown:
                print("unknown\(String(describing: assetExport?.error))")
                completion(nil)
            case AVAssetExportSessionStatus.waiting:
                print("waiting\(String(describing: assetExport?.error))")
            case AVAssetExportSessionStatus.exporting:
                print("exporting\(String(describing: assetExport?.error))")
            default:
                completion(mergeAudioURL)
                print("-----Merge audio exportation complete.\(mergeAudioURL)")
            }
        })
    }
    
    func removeFileAtURLIfExists(url: NSURL) {
        if let filePath = url.path {
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                do {
                    try fileManager.removeItem(atPath: filePath)
                } catch {
                    print("error")
                }
            }
        }
    }
}

public enum DisplayType {
    case unknown
    case iphone4
    case iphone5
    case iphone6
    case iphone6plus
    case iPadNonRetina
    case iPad
    case iPadProBig
    static let iphone7 = iphone6
    static let iphone7plus = iphone6plus
}

public final class Display {
    class var width: CGFloat { return UIScreen.main.bounds.size.width }
    class var height: CGFloat { return UIScreen.main.bounds.size.height }
    class var maxLength: CGFloat { return max(width, height) }
    class var minLength: CGFloat { return min(width, height) }
    class var zoomed: Bool { return UIScreen.main.nativeScale >= UIScreen.main.scale }
    class var retina: Bool { return UIScreen.main.scale >= 2.0 }
    class var phone: Bool { return UIDevice.current.userInterfaceIdiom == .phone }
    class var pad: Bool { return UIDevice.current.userInterfaceIdiom == .pad }
    class var carplay: Bool { return UIDevice.current.userInterfaceIdiom == .carPlay }
    class var tv: Bool { return UIDevice.current.userInterfaceIdiom == .tv }
    class var typeIsLike: DisplayType {
        if phone && maxLength < 568 {
            return .iphone4
        } else if phone && maxLength == 568 {
            return .iphone5
        } else if phone && maxLength == 667 {
            return .iphone6
        } else if phone && maxLength == 736 {
            return .iphone6plus
        } else if pad && !retina {
            return .iPadNonRetina
        } else if pad && retina && maxLength == 1024 {
            return .iPad
        } else if pad && maxLength == 1366 {
            return .iPadProBig
        }
        return .unknown
    }
}
