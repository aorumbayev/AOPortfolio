//
//  Constants.swift
//  ESREVER
//
//  Created by Altynbek Orumbayev on 6/26/17.
//  Copyright © 2017 Altynbek Orumbayev. All rights reserved.
//

import UIKit

struct Constants {
    static let circularBtnSize = (UIScreen.main.bounds.width/2) - 40
    static let screenWidth = UIScreen.main.bounds.width
    static let screenHeight = UIScreen.main.bounds.height
    static let shadowColor: CGFloat = 157.0 / 255.0

    static let isInDevMode = false
}

//static let Constants.circularBtnSize = (UIScreen.main.bounds.width/2) - 40
//static let Constants.screenWidth = UIScreen.main.bounds.width
//static let Constants.screenHeight = UIScreen.main.bounds.height
//static let Constants.shadowColor: CGFloat = 157.0 / 255.0
//
//static let isInDevMode = true
