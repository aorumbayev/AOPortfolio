//
//  ESTextInputController.swift
//  ESREVER
//
//  Created by Altynbek Orumbayev on 8/12/17.
//  Copyright © 2017 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import PureLayout
import UITextView_Placeholder
import AVKit

class ESTextInputController: UIViewController, UITextViewDelegate, ESAudioRecorderViewControllerDelegate {
    
    // MARK: - Actions
    
    var didUpdateConstraints = false
    var splittedWordsDictionary = [String: Int]()
    var wordUrlsDictionary = [String: URL]()
    var isInRecordingMode: Bool = false {
        didSet {
            self.recordedWordsLabel.alpha = CGFloat(truncating: NSNumber(value: isInRecordingMode))
            self.resetBtn.alpha = CGFloat(truncating: NSNumber(value: isInRecordingMode))
            self.attributedTextView.alpha = CGFloat(truncating: NSNumber(value: isInRecordingMode))
            
            self.charsLabel.alpha = CGFloat(truncating: NSNumber(value: !isInRecordingMode))
            self.doneBtn.alpha = CGFloat(truncating: NSNumber(value: !isInRecordingMode))
            self.inputTextView.alpha = CGFloat(truncating: NSNumber(value: !isInRecordingMode))
            
            self.curAmountOfRecordedWords = isInRecordingMode ? self.curAmountOfRecordedWords : 0
            
            isInRecordingMode ? self.resetBtn.pop() : self.doneBtn.pop()
            isInRecordingMode ? self.recordedWordsLabel.pop() : self.charsLabel.pop()
            isInRecordingMode ? self.attributedTextView.pop() : self.inputTextView.pop()
        }
    }
    
    var curAmountOfWords: Int = 0
    var curAmountOfRecordedWords: Int = 0 {
        didSet {
            let status = (curAmountOfRecordedWords == curAmountOfWords)
            UIView.animate(withDuration: 0.2) {
                self.nextBtn.alpha = CGFloat(truncating: NSNumber.init(value: status))
                self.nextBtn.pop()
            }
        }
    }
    
    let attributedTextView: UITextView = {
        let text = UITextView().configureForAutoLayout()
        text.textColor = .white
        text.font = UIFont(name: "AvantGarde-CondBook", size: 55)!
        text.tintColor = .red
        text.backgroundColor = .clear
        text.textAlignment = .center
        text.tag = 2
        text.isEditable = false
        text.alpha = 0.0
        return text
    }()
    
    let inputTextView: UITextView = {
        let text = UITextView().configureForAutoLayout()
        text.textColor = .white
        text.font = UIFont(name: "AvantGarde-CondBook", size: Display.typeIsLike == DisplayType.iphone5 ? 50 : 55)
        text.tintColor = .red
        text.returnKeyType = .done
        text.backgroundColor = .clear
        text.placeholder = "tap me then Type something meaningful!".uppercased()
        text.placeholderColor = .lightGray
        text.autocapitalizationType = UITextAutocapitalizationType.allCharacters
        text.textAlignment = .center
        text.tag = 1
        return text
    }()
    
    var backBtn: UIButton! = {
        var tempBackBtn = UIButton().configureForAutoLayout()
        tempBackBtn.setImage(#imageLiteral(resourceName: "backIcon").scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        tempBackBtn.imageView?.contentMode = .scaleAspectFit
        tempBackBtn.tintColor = UIColor.white
        tempBackBtn.addTarget(self, action: #selector(backBtnPressed), for: .touchUpInside)
        return tempBackBtn
    }()
    
    var doneBtn: UIButton! = {
        var tempDoneBtn = UIButton().configureForAutoLayout()
        tempDoneBtn.setImage(#imageLiteral(resourceName: "doneBtn").scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        tempDoneBtn.imageView?.contentMode = .scaleAspectFit
        tempDoneBtn.tintColor = UIColor.white
        tempDoneBtn.alpha = 0.0
        tempDoneBtn.addTarget(self, action: #selector(doneBtnPressed), for: .touchUpInside)
        return tempDoneBtn
    }()
    
    var nextBtn: UIButton! = {
        var tempNextBtn = UIButton().configureForAutoLayout()
        tempNextBtn.setImage(#imageLiteral(resourceName: "nextIcon").scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        tempNextBtn.imageView?.contentMode = .scaleAspectFit
        tempNextBtn.tintColor = UIColor.white
        tempNextBtn.alpha = 0
        tempNextBtn.addTarget(self, action: #selector(nextBtnPressed), for: .touchUpInside)
        return tempNextBtn
    }()
    
    var resetBtn: UIButton! = {
        var tempDoneBtn = UIButton().configureForAutoLayout()
        tempDoneBtn.setImage(#imageLiteral(resourceName: "resetIcon").scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        tempDoneBtn.imageView?.contentMode = .scaleAspectFit
        tempDoneBtn.tintColor = UIColor.white
        tempDoneBtn.alpha = 0.0
        tempDoneBtn.addTarget(self, action: #selector(resetBtnPressed), for: .touchUpInside)
        return tempDoneBtn
    }()
    
    let recordedWordsLabel: LTMorphingLabel = {
        
        let label = LTMorphingLabel().configureForAutoLayout()
        label.font = UIFont(name: "AvantGarde-CondBook", size: 25)
        label.morphingEffect = .scale
        label.numberOfLines = 1
        label.text = ""
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        label.morphingDuration = 0.75
        label.textColor = UIColor.white
        label.alpha = 0.0
        
        return label
    }()
    
    let charsLabel: LTMorphingLabel = {
        
        let label = LTMorphingLabel().configureForAutoLayout()
        label.text = "0 : 140"
        label.font = UIFont(name: "AvantGarde-CondBook", size: 25)
        label.morphingEffect = .scale
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        label.morphingDuration = 0.75
        label.textColor = UIColor.white
        
        return label
    }()
    
    var wordsArray = [String]()
    var bottomConstraint: NSLayoutConstraint!
    var bottomInputConstraint: NSLayoutConstraint!
    
    // MARK: - UIViewController
    
    override func loadView() {
        self.view = ESGradientView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Observe keyboard change
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        setupViews()
    }
    
    override func updateViewConstraints() {
        
        if !didUpdateConstraints {
            
            backBtn.autoPinEdge(toSuperviewEdge: .left, withInset: 15)
            backBtn.autoPinEdge(toSuperviewEdge: .top, withInset: 25)
            backBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))
            
            nextBtn.autoPinEdge(toSuperviewEdge: .right, withInset: 15)
            nextBtn.autoPinEdge(toSuperviewEdge: .bottom, withInset: 25)
            nextBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))
            
            doneBtn.autoPinEdge(toSuperviewEdge: .right, withInset: 15)
            doneBtn.autoPinEdge(toSuperviewEdge: .top, withInset: 25)
            doneBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))
            
            resetBtn.autoPinEdge(toSuperviewEdge: .right, withInset: 15)
            resetBtn.autoPinEdge(toSuperviewEdge: .top, withInset: 25)
            resetBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))
            
            charsLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 25)
            charsLabel.autoPinEdge(.left, to: .right, of: backBtn)
            charsLabel.autoPinEdge(.right, to: .left, of: doneBtn)
            charsLabel.autoAlignAxis(.horizontal, toSameAxisOf: doneBtn)
            
            recordedWordsLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 25)
            recordedWordsLabel.autoPinEdge(.left, to: .right, of: backBtn)
            recordedWordsLabel.autoPinEdge(.right, to: .left, of: doneBtn)
            recordedWordsLabel.autoAlignAxis(.horizontal, toSameAxisOf: doneBtn)
            
            attributedTextView.autoPinEdge(toSuperviewEdge: .left, withInset: 15)
            attributedTextView.autoPinEdge(toSuperviewEdge: .right, withInset: 15)
            attributedTextView.autoPinEdge(.top, to: .bottom, of: backBtn, withOffset: 30)
            bottomConstraint = attributedTextView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 25)
            
            inputTextView.autoPinEdge(toSuperviewEdge: .left, withInset: 15)
            inputTextView.autoPinEdge(toSuperviewEdge: .right, withInset: 15)
            inputTextView.autoPinEdge(.top, to: .bottom, of: backBtn, withOffset: 30)
            bottomInputConstraint = inputTextView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 25)
            
            didUpdateConstraints = true
        }
        
        super.updateViewConstraints()
    }
    
    // MARK: - Actions
    
    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func doneBtnPressed() {
        guard let curText = self.inputTextView.text as String? else {return}
        if (curText.count > 0) {
            self.wordsArray = curText.components(separatedBy: " ")
            let attributedString = NSMutableAttributedString(string: "")
            for index in 0..<wordsArray.count {
                let word = wordsArray[index]
                attributedString.append(turnStringIntoLink(inputString: word))
                attributedString.append(NSAttributedString(string: index >= wordsArray.count-1 ? "" : "   "))
                guard let range = attributedString.string.rangeOfNthWord(wordNum: index, wordSeparator: "   ") as NSRange? else {return}
                self.splittedWordsDictionary.updateValue(index, forKey: range.stringValue())
            }
            self.attributedTextView.attributedText = attributedString
            self.attributedTextView.tintColor = .white
            self.isInRecordingMode = true
            self.curAmountOfWords = wordsArray.count
            self.recordedWordsLabel.text = "0 : \(curAmountOfWords)"
        } else {
            self.isInRecordingMode = false
        }
    }
    
    func turnStringIntoLink(inputString: String) -> NSMutableAttributedString {
         // Turns a String into an NSMutableAttributedString
        let linkString = NSMutableAttributedString(string: inputString)
        let range = NSRange(location: 0, length: inputString.count)
        linkString.addAttribute(.link, value: inputString, range: range)
        linkString.addAttribute(.font, value: UIFont(name: "AvantGarde-CondBook", size: 50)!, range: range)
        linkString.addAttribute(.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        linkString.addAttribute(.underlineColor, value: UIColor.red, range: range)
        
        return linkString
    }
    
    @objc func resetBtnPressed() {
        self.isInRecordingMode = false
    }
    
    @objc func nextBtnPressed() {
        self.loadingView.startAnimating()
        let allAudioURLs = NSMutableArray()
        for word in self.splittedWordsDictionary.sorted(by: { (first, second) -> Bool in
            return first.value < second.value
        }) {
            if let url = self.wordUrlsDictionary[word.key] as URL? {
                allAudioURLs.add(url)
            }
        }
        
        self.mergeAudioFiles(audioFileUrls: allAudioURLs, interval: 0, completion: { (url) in
            DispatchQueue.main.async {
                self.loadingView.stopAnimating()
                if let mergedURL = url as URL? {
                    let shareVC = ESShareViewController()
                    shareVC.audioPath = mergedURL
                    self.navigationController?.pushViewController(shareVC, animated: true)
                }
            }
        })
    }
    
    func handleWordPressed(word: String, range: NSRange) {
        let audioVC = ESAudioRecorderViewController()
        audioVC.recordingWord = word
        audioVC.recordingWordRange = range
        audioVC.delegate = self
        self.navigationController?.pushViewController(audioVC, animated: true)
    }
    
    let loadingView: ESLoadingVIew = {
        return ESLoadingVIew.init(frame: CGRect.init(x: 0, y: 0, width: Constants.screenWidth, height: Constants.screenHeight))
    }()
    
    func setupViews() {
        self.view.addSubview(attributedTextView)
        self.view.addSubview(inputTextView)
        self.view.addSubview(nextBtn)
        self.view.addSubview(backBtn)
        self.view.addSubview(doneBtn)
        self.view.addSubview(charsLabel)
        self.view.addSubview(resetBtn)
        self.view.addSubview(recordedWordsLabel)
        self.view.addSubview(loadingView)
        
        self.inputTextView.delegate = self
        self.attributedTextView.delegate = self
        self.view.setNeedsUpdateConstraints()
    }
    
    func animateDoneButton(visible: Bool) {
        if (self.doneBtn.alpha != CGFloat(truncating: NSNumber(value: visible))) {
            UIView.animate(withDuration: 0.1) {
                self.doneBtn.alpha = CGFloat(truncating: NSNumber(value: visible))
                self.doneBtn.pop()
            }
        }
    }
    
    func updateRecordedWordsLabel() {
        if self.attributedTextView.attributedText.length >= 0 && isInRecordingMode {
            self.curAmountOfRecordedWords = self.wordUrlsDictionary.count
            self.recordedWordsLabel.text = "\(self.curAmountOfRecordedWords) : \(self.curAmountOfWords)"
        }
    }
    
    // MARK: - Keyboard Handling
    
    func keyboardWillShow(notification: NSNotification) {
        if let nInfo = (notification as NSNotification).userInfo, let value = nInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let frame = value.cgRectValue
            
            bottomConstraint.autoRemove()
            bottomConstraint = attributedTextView.autoPinEdge(toSuperviewEdge: .bottom, withInset: frame.height)
            
            bottomInputConstraint.autoRemove()
            bottomInputConstraint = inputTextView.autoPinEdge(toSuperviewEdge: .bottom, withInset: frame.height)
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let nInfo = (notification as NSNotification).userInfo, let value = nInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let frame = value.cgRectValue
            
            bottomConstraint.autoRemove()
            bottomConstraint = attributedTextView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 25)
            
            bottomInputConstraint.autoRemove()
            bottomInputConstraint = inputTextView.autoPinEdge(toSuperviewEdge: .bottom, withInset: 25)
        }
    }
    
    // MARK: - UITextViewDelegate
    
    @available(iOS, deprecated: 10.0)
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
        let string = url.absoluteString.replacingOccurrences(of: "AttributedTextView:", with: "")
        print(string)
        self.handleWordPressed(word: string, range: characterRange)
        return true
    }

    //For iOS 10
    @available(iOS 10.0, *)
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        let string = url.absoluteString.replacingOccurrences(of: "AttributedTextView:", with: "")
        print(string)
        self.handleWordPressed(word: string, range: characterRange)
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.tag == 1 {
            if text == "\n" {
                textView.resignFirstResponder()
            }
            return textView.text.count + text.count <= 140
        } else {
            return true
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        guard let curText = textView.text as String? else {return}
        textView.text = curText.uppercased()
        
        if textView.tag == 1 {
            if curText.count >= 0 {
                self.charsLabel.text = "\(curText.count) : 140"
                if isInRecordingMode {
                    self.recordedWordsLabel.text = "\(curText.count) : \(self.curAmountOfWords)"
                }
            }
            
            self.animateDoneButton(visible: curText.count > 0)
        }
    }
    
    // MARK: - ESAudioRecorderViewController
    func didRecordedAudio(word: String, wordRange: NSRange, player: AVAudioPlayer) {
        guard let audioUrl = player.url as URL? else {return}
        self.wordUrlsDictionary.updateValue(audioUrl, forKey: wordRange.stringValue())
        self.updateRecordedWordsLabel()
        let attributedString = NSMutableAttributedString(attributedString: self.attributedTextView.attributedText)
        attributedString.addAttributes([NSAttributedStringKey.underlineColor: UIColor.white], range: wordRange)
        self.attributedTextView.attributedText = attributedString
    }
}
