//
//  TutorialView.swift
//  Awardy
//
//  Created by Altynbek Orumbaev on 25.01.16.
//  Copyright © 2016 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import RazzleDazzle

class ESTutorialViewController: AnimatedPagingScrollViewController {
    
    let page1Logo = UIImageView(image: #imageLiteral(resourceName: "step1"))
    let page2Logo = UIImageView(image: #imageLiteral(resourceName: "step2"))
    let page3Logo = UIImageView(image: #imageLiteral(resourceName: "step3"))
    let page4Logo = UIImageView(image: #imageLiteral(resourceName: "step4"))
    let page5Logo = UIImageView(image: #imageLiteral(resourceName: "step5"))
    let page1Text = UIImageView(image: #imageLiteral(resourceName: "logo"))
    let page1TextLabel = UILabel()
    let page2Text = UILabel()
    let page3Text = UILabel()
    let page4Text = UILabel()
    let page5Text = UILabel()
    let loginBtn = UIButton()
    let skipBtn = UIButton()
    
    override func numberOfPages() -> Int {
        // Tell the scroll view how many pages it has
        return 5
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gradientView = ESGradientView(frame: self.view.bounds)
        self.view.insertSubview(gradientView, at: 0)
        
        configureViews()
        //      configureScrollView()
        configureFirstPage()
        configureSecondPage()
        configureThirdPage()
        configureFourthPage()
        configureFifthPage()
        animateCurrentFrame()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func configureViews() {
        // Add each of the views to the contentView
        contentView.addSubview(page1Logo)
        contentView.addSubview(page1Text)
        contentView.addSubview(page1TextLabel)
        contentView.addSubview(page2Text)
        contentView.addSubview(page2Logo)
        contentView.addSubview(page3Logo)
        contentView.addSubview(page3Text)
        contentView.addSubview(page4Logo)
        contentView.addSubview(page4Text)
        contentView.addSubview(page5Logo)
        contentView.addSubview(page5Text)
        contentView.addSubview(loginBtn)
        contentView.addSubview(skipBtn)
        
        page1Text.contentMode = .scaleAspectFit
        page1Logo.contentMode = .scaleAspectFit
        
        page1TextLabel.textColor = UIColor.white
        page1TextLabel.font = UIFont(name: "AvantGarde-CondBook", size: 25)
        page1TextLabel.numberOfLines = 2
        page1TextLabel.text = "Record your voice and create weird audio effect"
        page1TextLabel.adjustsFontSizeToFitWidth = true
        page1TextLabel.textAlignment = NSTextAlignment.center
        
        page2Logo.contentMode = .scaleAspectFit
        
        page2Text.textColor = UIColor.white
        page2Text.font = UIFont(name: "AvantGarde-CondBook", size: 25)
        page2Text.numberOfLines = 3
        page2Text.text = "Step 1.\nType the text that you want to record.\nStart tapping on words to record initial speech"
        page2Text.adjustsFontSizeToFitWidth = true
        page2Text.textAlignment = NSTextAlignment.center
        
        page3Logo.contentMode = .scaleAspectFit
        
        page3Text.textColor = UIColor.white
        page3Text.font = UIFont(name: "AvantGarde-CondBook", size: 25)
        page3Text.numberOfLines = 3
        page3Text.text = "Step 2.\nRemember, repeat and record reversed speech. Make sure your device is not in silent mode!"
        page3Text.adjustsFontSizeToFitWidth = true
        page3Text.textAlignment = NSTextAlignment.center
        
        page4Logo.contentMode = .scaleAspectFit
        
        page4Text.textColor = UIColor.white
        page4Text.font = UIFont(name: "AvantGarde-CondBook", size: 25)
        page4Text.numberOfLines = 3
        page4Text.text = "Step 3.\nListen and edit reversed version of reversed speech. Hit next when all words are recorded"
        page4Text.adjustsFontSizeToFitWidth = true
        page4Text.textAlignment = NSTextAlignment.center
        
        page5Logo.contentMode = .scaleAspectFit
        
        page5Text.textColor = UIColor.white
        page5Text.font = UIFont(name: "AvantGarde-CondBook", size: 25)
        page5Text.numberOfLines = 3
        page5Text.text = "Step 4.\nShare with your Friends 😀"
        page5Text.adjustsFontSizeToFitWidth = true
        page5Text.textAlignment = NSTextAlignment.center
        
        loginBtn.setTitle("LET'S ROCK!", for: .normal)
        loginBtn.setTitleColor(UIColor.white, for: .normal)
        loginBtn.titleLabel!.font =  UIFont(name: "AvantGarde-CondBook", size: 20)
        loginBtn.layer.cornerRadius = 5
        loginBtn.setTitleColor(UIColor.lightGray, for: .highlighted)
        loginBtn.backgroundColor = .black
        loginBtn.layer.borderWidth = 2
        loginBtn.layer.borderColor = UIColor.white.cgColor
        loginBtn.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        loginBtn.addTarget(self, action: #selector(skipBtnPressed(_:)), for: .touchUpInside)
    }
    
    @objc func skipBtnPressed(_ sender: AnyObject) {
        UserDefaults.standard.setValue(true, forKey: "firstLaunch")
        let vc = ESMenuViewController()
        let navVC = UINavigationController.init(rootViewController: vc)
        navVC.setNavigationBarHidden(true, animated: false)
        AppDelegate.replaceRootController(newViewController: navVC)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
