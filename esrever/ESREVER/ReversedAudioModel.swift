//
//  ReversedAudioModel.swift
//  ESREVER
//
//  Created by Altynbek Orumbayev on 7/10/17.
//  Copyright © 2017 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import AVFoundation

class ReversedAudioModel {
    enum State {
        case loading
        case error
        case ready
    }
    
    var forwardURL: URL?
    var backwardURL: URL?
    
    var onStateChange : ((State) -> Void)?
    var state : State = .loading {
        didSet {
            onStateChange?(state)
        }
    }
    
    init (source: URL) {
        let tempDirURL: URL = URL(fileURLWithPath: NSTemporaryDirectory())
        let stripExtension = source.deletingPathExtension()
        let filenameStub = stripExtension.pathComponents.last!
        
        forwardURL = tempDirURL.appendingPathComponent(filenameStub + "-forward.m4a")
        var tempBackward = tempDirURL.appendingPathComponent(filenameStub + "-tempBackward.m4a")
        backwardURL = tempDirURL.appendingPathComponent(filenameStub + "-backward.m4a")
        
        DispatchQueue.global(qos: .default).async {
            var err: OSStatus = noErr
            err = convertAndReverseSwift(sourceURL: source as CFURL,
                                         forwardURL: self.forwardURL! as CFURL,
                                         backwardURL: tempBackward as CFURL)
            let converter = ExtAudioConverter()
            converter.inputFile = tempBackward.path
            converter.outputFile = self.backwardURL?.path
            converter.outputFormatID = kAudioFormatAppleLossless
            converter.outputFileType = kAudioFileM4AType
            converter.convert()
            self.backwardURL = URL(fileURLWithPath: converter.outputFile)
            print ("converter done, err is \(err)")
            self.state = (err == noErr) ? .ready : .error
        }
    }
}
