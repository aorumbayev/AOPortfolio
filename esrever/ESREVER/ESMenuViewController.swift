//
//  AKMenuViewController.swift
//  ESREVER
//
//  Created by Altynbek Orumbayev on 6/26/17.
//  Copyright © 2017 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import TOMSMorphingLabel

class ESMenuViewController: UIViewController {

    var backgroundGradientView: ESGradientView!
    var recordButton: ESPopButton!
    var slider: UISlider!
    var didUpdateConstraints = false
    var infoBtn = UIButton()
    var tutorialBtn = UIButton()

    lazy var startButtonLabel: TOMSMorphingLabel! = {
        var tempStartButtonLabel = TOMSMorphingLabel(frame: CGRect.init(x: 0, y: 0, width: Constants.screenWidth / 1.5, height: 40))
        tempStartButtonLabel.textColor = .white
        tempStartButtonLabel.textAlignment = .center
        tempStartButtonLabel.characterShrinkFactor = 2
        tempStartButtonLabel.font = UIFont(name: "AvantGarde-CondBook", size: 18)

        tempStartButtonLabel.setTextWithoutMorphing("Let's rock!".uppercased())
        return tempStartButtonLabel
    }()

    lazy var logoLabel: UILabel! = {
        var tempLogoLabel = UILabel(frame: CGRect(x: 0, y: 0, width: Constants.screenWidth / 1.2, height: 40))
        tempLogoLabel.textColor = .white
        tempLogoLabel.textAlignment = .center
        tempLogoLabel.font = UIFont(name: "AvantGarde-CondBook", size: 40)
        tempLogoLabel.text = "ESREVER"
        return tempLogoLabel
    }()

    override func loadView() {
        self.view = ESGradientView(frame: CGRect.init(x: 0, y: 0, width: Constants.screenWidth, height: Constants.screenHeight))
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        self.view.backgroundColor = .black
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setMorphingText(text: "Let's rock!".uppercased(), nextText: "!kcor s'teL".uppercased())
    }

    override func updateViewConstraints() {
        if !didUpdateConstraints {

            self.infoBtn.autoPinEdge(toSuperviewEdge: .right, withInset: 15)
            self.infoBtn.autoPinEdge(toSuperviewEdge: .top, withInset: 15)
            self.infoBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))

            self.tutorialBtn.autoPinEdge(toSuperviewEdge: .left, withInset: 15)
            self.tutorialBtn.autoPinEdge(toSuperviewEdge: .top, withInset: 15)
            self.tutorialBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))

            self.didUpdateConstraints = true
        }

        super.updateViewConstraints()
    }

    // MARK: - Actions

    func setupViews() {
        self.backgroundGradientView = ESGradientView(frame: CGRect.init(x: 0, y: 0, width: Constants.screenWidth, height: Constants.screenHeight))
        self.view.addSubview(self.backgroundGradientView)

        recordButton = ESPopButton.init(frame:CGRect.init(x: 0, y: 0, width: Constants.circularBtnSize, height: Constants.circularBtnSize))
        recordButton.setImage(#imageLiteral(resourceName: "startButton"), for: .normal)
        recordButton.imageView?.contentMode = .scaleAspectFit
        recordButton.addTarget(self, action: #selector(presentAudioRecorder(_:)), for: .touchUpInside)
        recordButton.layer.position = CGPoint.init(x: Constants.screenWidth/2, y: Constants.screenHeight/2)
        self.view.addSubview(recordButton)

        startButtonLabel.layer.anchorPoint = CGPoint(x: 0.5, y: 0)
        startButtonLabel.layer.position = CGPoint(x: recordButton.layer.position.x,
                                                  y: (recordButton.layer.position.y + recordButton.frame.size.height/2 + 10))
        self.view.addSubview(startButtonLabel)

        self.infoBtn.setImage(#imageLiteral(resourceName: "info").scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.infoBtn.imageView?.contentMode = .scaleAspectFit
        self.infoBtn.tintColor = UIColor.white
        self.infoBtn.addTarget(self, action: #selector(infoBtnPressed), for: .touchUpInside)
        self.view.addSubview(infoBtn)

        self.tutorialBtn.setImage(#imageLiteral(resourceName: "question").scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.tutorialBtn.imageView?.contentMode = .scaleAspectFit
        self.tutorialBtn.tintColor = UIColor.white
        self.tutorialBtn.addTarget(self, action: #selector(tutorialBtnPressed), for: .touchUpInside)
        self.view.addSubview(tutorialBtn)

        self.updateViewConstraints()
      }

    func setMorphingText(text: String!, nextText: String!) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
            self.startButtonLabel.setText(text) {
                self.setMorphingText(text: nextText, nextText: text)
            }
        }
    }

    @objc func infoBtnPressed() {
        self.navigationController?.pushViewController(ESInfoViewController(), animated: true)
    }

    @objc func tutorialBtnPressed() {
        AppDelegate.replaceRootController(newViewController: ESTutorialViewController())
    }

    func startColorAnimations() {
        turnBlack()
    }

    func turnRed() {
        UIView.animate(withDuration: 20, delay: 0, options: .curveEaseInOut, animations: {
            self.view.backgroundColor = UIColor(r: 135, g: 0, b: 1)
        }) { (_) in
            self.turnBlack()
        }
    }

    func turnBlack() {
        UIView.animate(withDuration: 20, delay: 0, options: .curveEaseInOut, animations: {
            self.view.backgroundColor = .black
        }) { (_) in
            self.turnRed()
        }
    }

    // MARK: - Buttons tap handlers

    @objc func presentAudioRecorder(_ sender: UIButton) {
        self.navigationController?.pushViewController(ESTextInputController(), animated: true)
    }

}
