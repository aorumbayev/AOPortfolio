//
//  ESInfoViewController.swift
//  ESREVER
//
//  Created by Altynbek Orumbayev on 7/11/17.
//  Copyright © 2017 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import PureLayout
import CRParticleEffect

class ESInfoViewController: UIViewController, CRMagicPanGestureRecognizerDelegate {

    lazy var backBtn: UIButton! = {
        var tempBackBtn = UIButton().configureForAutoLayout()
        tempBackBtn.setImage(#imageLiteral(resourceName: "backIcon").scaleImage(toSize: CGSize(width: 25, height: 25))?.withRenderingMode(.alwaysTemplate), for: .normal)
        tempBackBtn.imageView?.contentMode = .scaleAspectFit
        tempBackBtn.tintColor = UIColor.white
        tempBackBtn.addTarget(self, action: #selector(backBtnPressed), for: .touchUpInside)
        return tempBackBtn
    }()

    var infoView: UIView!
    var didUpdateConstraints = false

    override func updateViewConstraints() {

        if !didUpdateConstraints {
            self.infoView.autoPinEdgesToSuperviewEdges()

            self.backBtn.autoPinEdge(toSuperviewEdge: .left, withInset: 15)
            self.backBtn.autoPinEdge(toSuperviewEdge: .top, withInset: 25)
            self.backBtn.autoSetDimensions(to: CGSize(width: 25, height: 25))

            self.didUpdateConstraints = true
        }

        super.updateViewConstraints()
    }

    override func loadView() {
        self.view = ESGradientView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.infoView = UIView.loadFromNibNamed(nibNamed: "ESInfoView", bundle: Bundle.main)?.configureForAutoLayout()
        self.view.addSubview(infoView)
        self.view.addSubview(backBtn)
        self.updateViewConstraints()
        
        let gestureRecognizer = CRMagicPanGestureRecognizer()
        gestureRecognizer.addTarget(self, action: #selector(pannedAction))
        self.view.addGestureRecognizer(gestureRecognizer)
        gestureRecognizer.delegate = self
        // Do any additional setup after loading the view.
    }

    @objc func backBtnPressed() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func pannedAction() {
        
    }
    
    // MARK: - CRMagicPanGestureRecognizerDelegate

    func gestureRecognizer(_ gestureRecognizer: CRMagicPanGestureRecognizer, particleEffectLayerFor touch: UITouch, with index: UInt) -> CRParticleEffectLayer {
        return CRParticleEffectLayer.init(images: [#imageLiteral(resourceName: "particleEmoji")])
    }
}
