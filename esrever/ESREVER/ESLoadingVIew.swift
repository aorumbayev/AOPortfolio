//
//  ESLoadingVIew.swift
//  ESREVER
//
//  Created by Altynbek Orumbayev on 8/20/17.
//  Copyright © 2017 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ESLoadingVIew: UIView {

    let activityIndicator: NVActivityIndicatorView = {
        let indicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width:50, height:50),
                                       type: NVActivityIndicatorType.orbit,
                                       color: UIColor.white,
                                       padding: 0)
        indicator.anchorPos = CGPoint.init(x: 0.5, y: 0.5)
        indicator.pos = CGPoint.init(x: Constants.screenWidth/2, y: Constants.screenHeight/2)
        return indicator
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
        self.alpha = 0
        self.addSubview(self.activityIndicator)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func startAnimating() {
        UIView.animate(withDuration: 0.25, animations: ({
            self.alpha = 1
        }))
        self.activityIndicator.startAnimating()
    }
    
    func stopAnimating() {
        UIView.animate(withDuration: 0.25, animations: ({
            self.alpha = 0
        }))
        self.activityIndicator.stopAnimating()
    }

}
