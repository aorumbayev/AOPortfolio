<p align="center">
  <img src="media/logo.jpg">
</p>

<p align="center">
    <img align="center" src="https://img.shields.io/badge/license-MIT-blue.svg"/>
</p>

### About

This repository is a code portfolio of Altynbek Orumbayev's iOS apps and projects.

#### List of projects sorted by year

- [2015](#section1)
  - [Castitude](#project1)
- [2016](#section2)
  - [GIFNSAPS](#project2)
- [2017](#section3)
  - [ESREVER](#project3)
- [Further development](#section4)

---

### 2015 <a id="section1"></a>

## Castitude <a id="project1"></a>

#### A minimalistic weather app

<p align="center">
  <img width="200px" src="media/castitude.gif">
</p>

### Description

`Castitude` : One of the earliest apps developed by me that made it into an
app store. The app was designed and created during an individual hackathon
session. The app represents simple Weather Forecast for current location or
any city around the world. Data is based on the `OpenWeatherMap` api.

### Credits

This software uses code from several open source packages:
• `Alamofire`
• `Spring`
• `Toucan`
• `IQKeyboardManagerSwift`

---

### 2016 <a id="section2"></a>

## GIFSNAPS <a id="project2"></a>

#### A trivia game with built-it computer vision

<p align="center">
  <img width="200px" src="media/gifsnaps.gif">
</p>

### Description

`GIFSNAPS` : The app was designed and created during an individual hackathon
session. It utilizes the `Microsoft Cognitive Vision` API for extracting objects
from an uploaded picture. Based on that object a request to `Giphy.com` is being
send. Then first player has to select 4 gifs that represents the object in a best way.
The second player has to guess the name of the object based on 4 gifs.

### Credits

This software uses code from several open source packages:
• `NVActivityIndicatorView`
• `PureLayout`
• `Gifu`
• `Kingfisher`
• `RazzleDazzle`

---

### 2017 <a id="section3"></a>

## ESREVER <a id="project3"></a>

#### Reversed audio speech recorder

<p align="center">
  <img width="200px" src="media/esrever.gif">
</p>

### Description

`ESREVER` : The app was designed and created during an individual hackathon session.
It is mainly created for a `Twin Peaks` tv series fanbase.
It allows user to create the same audio effect that was used during the famous
`Red Room` scenes in original first two season made in 90x as well
as the latest third season.

### Credits

This software uses code from several open source packages:
• `PureLayout`
• `RazzleDazzle`
• `IQKeyboardManagerSwift`
• `UITextView+Placeholder`
• `TOMSMorphingLabel`
• `SwiftySound`
• `CRParticleEffect`
• `NVActivityIndicatorView`
• `IQAudioRecorderController`
• `SwiftLint`

---

## Further development <a id="section4"></a>

The following repository is actively updated with new projects as well as incremental fixes for current projects that are hosted in the repository. Swift community is evolving very rapidly, therefore regular updates are performed in order to be available on latest iterations of `Swift` and `Xcode`.

Bugs might appear due to the updates in language, xcode and pod frameworks, but they are monitored and I usually update it several times per month.

All of the provided applications were available in the `AppStore` for each year stated in the content section. Due to the fact that they moved into portfolio repository, analytics frameworks as well as minor advertisement frameworks were removed. Therefore, they are no longer available for app store distribution.
