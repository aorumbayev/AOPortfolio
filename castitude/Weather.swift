//
//  Weather.swift
//  OneTapWeather
//
//  Created by Altynbek Orumbaev on 19.12.15.
//  Copyright © 2015 Altynbek Orumbaev. All rights reserved.
//

import Foundation
import Alamofire


class Weather {
    
    fileprivate var _cityName: String! = ""
    fileprivate var _cityID: String! = ""
    fileprivate var _geoCoordinates: String! = ""
    fileprivate var _weatherUrl: String! = ""
    fileprivate var _temperature: Int! = 0
    fileprivate var _countryID: String! = ""
    fileprivate var _errorStatus: Bool = false
    fileprivate var _pressure: Int = 0
    fileprivate var _wind: Int = 0
    fileprivate var _humidity: Int = 0
    fileprivate var _internetAccess: Bool = false
    
    var weatherUrl: String {
        
        get {
            return _weatherUrl
        }
        
        set (newUrl) {
            
            _weatherUrl = newUrl
            
        }
        
    }
    
    var internetAccess: Bool {
        
        get {
            return _internetAccess
        }
        
        set (internetAccess) {
            
            _internetAccess = internetAccess
            
        }
    }
    
    var errorStatus: Bool {
        
        get {
            return _errorStatus
        }
        
        set (error) {
            
            _errorStatus = error
            
        }
    }
    
    var countryID: String {
        
        get {
            return _countryID
        }
        
        set (newCountryID) {
            
            _countryID = newCountryID
            
        }
    }
    
    var cityName: String {
        
        get {
            return _cityName
        }
        
        set (newName) {
            
            _cityName = newName
            
        }
    }
    
    var temperature : Int {
        get {
            return _temperature
        }
        set (newTempVal) {
            
            _temperature = newTempVal
            
        }
    }
    
    
    var wind : Int {
        
        get {
            return _wind
        }
        
        set (newWind) {
            
            _wind = newWind
            
        }
    }
    
    var pressure : Int {
        get {
            return _pressure
        }
        set (newPressure) {
            
            _pressure = newPressure
            
        }
    }
    
    var humidity : Int {
        get {
            return _humidity
        }
        set (newHumid) {
            
            _humidity = newHumid
            
        }
    }
    
    init() {}
    
    init(lattitude: Double, longitude: Double) {
        
        if (isMetric) {
            
        weatherUrl = "\(WEATHER_URL_BY_COORDINATES_LATTITUDE)\(lattitude)\(WEATHER_URL_BY_COORDINATES_LONGITUDE)\(longitude)&units=metric\(API_KEY)"
            
        }
        
        else {
         
            weatherUrl = "\(WEATHER_URL_BY_COORDINATES_LATTITUDE)\(lattitude)\(WEATHER_URL_BY_COORDINATES_LONGITUDE)\(longitude)&units=imperial\(API_KEY)"
            
        }
        //weatherUrl = "http://api.openweathermap.org/data/2.5/weather?lat=43.4205318624008&lon=77.0223844379126&appid=7ff7443b07ddffb620c5071bc4b391e5"
    }
    
    init(cityName: String, countryName: String) {
        
        if (isMetric) {
            
            weatherUrl = "\(WEATHER_URL_BY_NAME)\(cityName),\(countryName.lowercased())&units=metric\(API_KEY)"
        
        }
            
        else {
            
            weatherUrl = "\(WEATHER_URL_BY_NAME)\(cityName),\(countryName.lowercased())&units=imperial\(API_KEY)"
            
        }
    }
    
    init(cityName: String) {
        
        if (isMetric) {
            
            weatherUrl = "\(WEATHER_URL_BY_NAME)\(cityName)&units=metric\(API_KEY)"
            
        }
            
        else {
            
            weatherUrl = "\(WEATHER_URL_BY_NAME)\(cityName)&units=imperial\(API_KEY)"
            
        }
        
    }
    
    func downloadWeatherDetails(_ completed: @escaping DownloadComplete) {
        
        let urlStr : NSString = weatherUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)! as NSString
        let url = URL(string: urlStr as String)!
        
        //let url  = NSURL(string: weatherUrl)!
        //print(weatherUrl)
        
        Alamofire.request(url).responseJSON {
            
            responce in
            
            let result = responce.result
            
            if String.init(result.description).contains("FAILURE") == false {
                
                if let mainDi = result.value as? Dictionary<String,AnyObject> {
                    
                    if let temp = mainDi["main"] as? Dictionary<String,AnyObject> {
                        
                        if let val = temp["temp"] as? NSNumber  {
                            self.temperature = val.intValue
                        }
                        
                        if let val = temp["pressure"] as? NSNumber  {
                            self.pressure = val.intValue
                        }
                        
                        if let val = temp["humidity"] as? NSNumber  {
                            self.humidity = val.intValue
                        }
                    }
                    
                    if let name = mainDi["name"] as? String {
                        self.cityName = name
                        self.errorStatus = false
                    }
                    
                    if let temp = mainDi["sys"] as? Dictionary<String,AnyObject> {
                        if let country = temp["country"] as? String  {
                            self._countryID = country
                        }
                    }
                    
                    if let temp = mainDi["wind"] as? Dictionary<String,AnyObject> {
                        if let val = temp["speed"] as? NSNumber  {
                            self.wind = val.intValue
                        }
                    }
                    
                    if let error = mainDi["message"] as? String {
                        if error == "Error: Not found city" || error.contains("Invalid API key") {
                            self.errorStatus = true
                        }
                    }
                    
                }
                
                self.internetAccess = true
            }
                
            else {
                
                self.internetAccess = false
            }
            
            
            
            completed()
            
        }
        
    }
}
