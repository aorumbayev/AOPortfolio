//
//  NetworkActivityManager.swift
//  Awardy
//
//  Created by Altynbek Orumbaev on 21.02.16.
//  Copyright © 2016 Altynbek Orumbayev. All rights reserved.
//

import Foundation
import UIKit

class NetworkActivityIndicatorManager: NSObject {
    
    fileprivate static var loadingCount = 0
    
    class func networkOperationStarted() {
        
        if loadingCount == 0 {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        loadingCount += 1
    }
    
    class func networkOperationFinished() {
        if loadingCount > 0 {
            loadingCount -= 1
        }
        if loadingCount == 0 {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
}
