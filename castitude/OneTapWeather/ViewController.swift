//
//  ViewController.swift
//  OneTapWeather
//
//  Created by Altynbek Orumbaev on 19.12.15.
//  Copyright © 2015 Altynbek Orumbaev. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import Spring
import Toucan

extension UIView {
    
    func pb_takeSnapshot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}


class ViewController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate, UICollectionViewDelegate,
UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var weatherObject : Weather!
    let locationManager = CLLocationManager()
    var tempCityName: String = ""
    var curLattitude: CLLocationDegrees = 0.0, curLongitude: CLLocationDegrees = 0.0
    var receivedLocation : CLLocation! = nil
    var locationStatus: String = ""
    var keyboardStatus = false
    //  var interstitial: GADInterstitial?
    
    
    var ignoreReceivedLocation = false
    var curRequestStatus = 0
    
    @IBOutlet weak var CountryNameLabel: SpringLabel!
    @IBOutlet weak var CityNameTextField: SpringTextField!
    @IBOutlet weak var PressureLabel: SpringLabel!
    @IBOutlet weak var WindLabel: SpringLabel!
    @IBOutlet weak var HumidityLabel: SpringLabel!
    @IBOutlet weak var MainStack: UIStackView!
    @IBOutlet weak var LocationButton: SpringButton!
    @IBOutlet weak var SaveButton: SpringButton!
    @IBOutlet weak var WindIImage: SpringImageView!
    @IBOutlet weak var HumidityImage: SpringImageView!
    @IBOutlet weak var PressureImage: SpringImageView!
    @IBOutlet weak var RefreshButton: SpringButton!
    @IBOutlet weak var ImageButton: SpringButton!
    @IBOutlet weak var HumidityNameLabel: SpringLabel!
    @IBOutlet weak var WindNameLabel: SpringLabel!
    @IBOutlet weak var PressureNameLabel: SpringLabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var curTempLBL: SpringLabel!
    
    
    var threeHourForecast: ThreeHourForecast?
    
    
    func updateMaterialDetails() {
        
        
        
        ImageButton.layer.allowsEdgeAntialiasing = true
        RefreshButton.layer.allowsEdgeAntialiasing = true
        LocationButton.layer.allowsEdgeAntialiasing = true
        SaveButton.layer.allowsEdgeAntialiasing = true
        PressureImage.layer.allowsEdgeAntialiasing = true
        HumidityImage.layer.allowsEdgeAntialiasing = true
        WindIImage.layer.allowsEdgeAntialiasing = true
        //.layer.allowsEdgeAntialiasing = true
        
        CityNameTextField.layer.cornerRadius = 2.0
        CityNameTextField.layer.shadowColor = UIColor.init(red: SHADOW_COLOR, green: SHADOW_COLOR, blue: SHADOW_COLOR, alpha: 0.5).cgColor
        CityNameTextField.layer.shadowOpacity = 0.8
        CityNameTextField.layer.shadowRadius = 5.0
        CityNameTextField.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        CityNameTextField.layer.allowsEdgeAntialiasing = true
        
        CountryNameLabel.layer.cornerRadius = 2.0
        CountryNameLabel.layer.shadowColor = UIColor.init(red: SHADOW_COLOR, green: SHADOW_COLOR, blue: SHADOW_COLOR, alpha: 0.5).cgColor
        CountryNameLabel.layer.shadowOpacity = 0.8
        CountryNameLabel.layer.shadowRadius = 5.0
        CountryNameLabel.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        CountryNameLabel.layer.allowsEdgeAntialiasing = true
        
        
    }
    
    // TODO: Replace this test id with your personal ad unit id
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        CityNameTextField.delegate = self
        
        
        self.locationManager.requestWhenInUseAuthorization()
        
        
        updateMaterialDetails()
        
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            initialSetup()
            locationManager.startUpdatingLocation()
        }
        
//        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
//        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
//        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keybordWillHideBackground(_:)), name:NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        
        //        if NSUserDefaults.standardUserDefaults().valueForKey("purchased") == nil {
        //            loadInterstitial()
        //        }
    }
    
    fileprivate func initialSetup() {
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: 60, height: 100)
        flowLayout.scrollDirection = .horizontal
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.collectionViewLayout = flowLayout
        collectionView.showsHorizontalScrollIndicator = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        UIApplication.shared.isStatusBarHidden = true
        self.navigationController?.navigationBar.isHidden = true
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            ignoreReceivedLocation = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if weatherObject != nil {
            
            getWeatherByName(CityNameTextField.text!, countryName: CountryNameLabel.text!)
            
        }
    }
    
    func requestLocation(){
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestLocation()
        }
    }
    
    
    func updateBackgroundColor(){
        
        if weatherObject.internetAccess == true {
            
            if weatherObject.errorStatus == false {
                
                if isMetric {
                    
                    if weatherObject.temperature <= 0 {
                        self.view.backgroundColor = colorsDictionary["Cold"]
                    }
                    else if weatherObject.temperature > 0 && weatherObject.temperature < 15 {
                        
                        self.view.backgroundColor = colorsDictionary["Middle"]
                    }
                    else {
                        self.view.backgroundColor = colorsDictionary["Hot"]
                    }
                }
                else {
                    if weatherObject.temperature <= 32 {
                        self.view.backgroundColor = colorsDictionary["Cold"]
                    }
                    else if weatherObject.temperature > 32 && weatherObject.temperature < 59 {
                        
                        self.view.backgroundColor = colorsDictionary["Middle"]
                    }
                    else {
                        self.view.backgroundColor = colorsDictionary["Hot"]
                    }
                }
                curColor = self.view.backgroundColor!
            }
        }
    }
    
    func updateWeather(){
        
        if weatherObject.internetAccess == true {
            
            if weatherObject.errorStatus == false {
                
                if weatherObject.temperature < 0 {
                    curTempLBL.text = String(weatherObject.temperature)+"°" }
                else {
                    curTempLBL.text = " "+String(weatherObject.temperature)+"°" }
                
                
                WindLabel.text = "\(weatherObject.wind) m/s"
                PressureLabel.text = "\(weatherObject.pressure) hpa"
                HumidityLabel.text = "\(weatherObject.humidity) %"
                CityNameTextField.text = weatherObject.cityName
                CountryNameLabel.text = weatherObject.countryID
            }
                
            else if weatherObject.errorStatus == true {
                
                CountryNameLabel.text = "Invalid City Name!"
                
            }
        }
            
        else  {
            
            CountryNameLabel.text = "Check your Internet connection!"
            
        }
        
        
        
    }
    
    func refreshAnimations(){
        
        
        curTempLBL.animation = "zoomIn"
        curTempLBL.duration = 1
        curTempLBL.animate()
        
        PressureImage.animation = "zoomIn"
        PressureImage.duration = 1
        PressureImage.animate()
        
        PressureNameLabel.animation = "fadeIn"
        PressureNameLabel.duration = 1
        PressureNameLabel.animate()
        
        PressureLabel.animation = "fadeIn"
        PressureLabel.duration = 1
        PressureLabel.animate()
        
        WindIImage.animation = "zoomIn"
        WindIImage.delay = 0.25
        WindIImage.duration = 1
        WindIImage.animate()
        
        WindNameLabel.animation = "fadeIn"
        WindNameLabel.delay = 0.25
        WindNameLabel.duration = 1
        WindNameLabel.animate()
        
        WindLabel.animation = "fadeIn"
        WindLabel.delay = 0.25
        WindLabel.duration = 1
        WindLabel.animate()
        
        
        HumidityImage.animation = "zoomIn"
        HumidityImage.delay = 0.5
        HumidityImage.duration = 1
        HumidityImage.animate()
        
        HumidityNameLabel.animation = "fadeIn"
        HumidityNameLabel.delay = 0.5
        HumidityNameLabel.duration = 1
        HumidityNameLabel.animate()
        
        HumidityLabel.animation = "fadeIn"
        HumidityLabel.delay = 0.5
        HumidityLabel.duration = 1
        HumidityLabel.animate()
        
        CityNameTextField.animation = "fadeIn"
        CityNameTextField.duration = 1
        CityNameTextField.delay = 0.25
        CityNameTextField.animate()
        
        CountryNameLabel.animation = "fadeIn"
        CountryNameLabel.duration = 1
        CountryNameLabel.delay = 0.5
        CountryNameLabel.animate()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if ignoreReceivedLocation == false {
            switch curRequestStatus {
            case 0 :
                
                getWeatherByCoordinates((locationManager.location?.coordinate.longitude)!, lattitude: (locationManager.location?.coordinate.latitude)!)
            default :
                break
            }
            
            locationManager.stopUpdatingLocation()
            
            ignoreReceivedLocation = true
            
        }
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        var shouldIAllow = false
        
        switch status {
        case CLAuthorizationStatus.restricted:
            locationStatus = "Restricted Access to location"
        case CLAuthorizationStatus.denied:
            locationStatus = "User denied access to location"
        case CLAuthorizationStatus.notDetermined:
            locationStatus = "Status not determined"
        default:
            shouldIAllow = true
        }
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "LabelHasbeenUpdated"), object: nil)
        if (shouldIAllow == true) {
            NSLog("Location to Allowed")
            // Start location services
            locationManager.startUpdatingLocation()
        } else {
            NSLog("Denied access: \(locationStatus)")
        }
    }
    
    func getWeatherByCoordinates (_ longitude: CLLocationDegrees, lattitude: CLLocationDegrees) {
        
        weatherObject = Weather(lattitude: lattitude, longitude: longitude)
        
        weatherObject.downloadWeatherDetails({ () -> () in
            
            self.updateWeather()
            
            self.threeHourForecast = ThreeHourForecast(lattitude: lattitude, longitude: longitude)
            
            if self.threeHourForecast != nil {
                
                self.threeHourForecast?.getDailyForecast({ () -> () in
                    self.collectionView.reloadData()
                    self.updateBackgroundColor()
                })
            }
        })
    }
    
    func getWeatherByName (_ cityName: String, countryName: String) {
        
        
        weatherObject = Weather(cityName: cityName, countryName: countryName)
        
        weatherObject.downloadWeatherDetails({ () -> () in
            
            self.updateWeather()
            
            self.threeHourForecast = ThreeHourForecast(cityName: cityName, countryName: countryName)
            
            if self.threeHourForecast != nil {
                
                self.threeHourForecast?.getDailyForecast({ () -> () in
                    self.collectionView.reloadData()
                    self.updateBackgroundColor()
                })
            }
        })
        
        
        
        
        
        
        
    }
    
    func getWeatherByLabel (_ cityName: String) {
        
        
        weatherObject = Weather(cityName: cityName)
        
        weatherObject.downloadWeatherDetails({ () -> () in
            
            self.updateWeather()
            
            self.threeHourForecast = ThreeHourForecast(cityName: cityName)
            
            if self.threeHourForecast != nil {
                
                self.threeHourForecast?.getDailyForecast({ () -> () in
                    self.collectionView.reloadData()
                    self.updateBackgroundColor()
                })
            }
        })
        
        
        
    }
    
    @IBAction func refreshBtnPressed(_ sender: AnyObject) {
        
        curRequestStatus = 1
        
        
        if let name = self.CityNameTextField.text {
            if let countryName = self.CountryNameLabel.text {
                
                getWeatherByName(name, countryName: countryName)
            }
        }
        
        
    }
    
    @IBAction func locationBtnPressed(_ sender: AnyObject) {
        // requestLocation()
        
        curRequestStatus = 0
        
        ignoreReceivedLocation = false
        locationManager.startUpdatingLocation()
    }
    
    @IBAction func saveBtnPressed(_ sender: UIButton) {
        
        let title = "Current weather in "+self.CityNameTextField.text!+"\n'Castitude' - download for FREE\nhttps://itunes.apple.com/us/app/castitude/id1071004700?l=ru&ls=1&mt=8"
        let screenshot: UIImage = (self.view?.pb_takeSnapshot())!
        let w = self.view.bounds.width, h = self.view.bounds.height - 90
        let croppedSc = Toucan(image: screenshot).resizeByCropping(CGSize(width:w, height: h)).image
        
        // create the controller
        let activityViewController = UIActivityViewController(activityItems: [title,croppedSc!], applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivityType.print, UIActivityType.postToWeibo, UIActivityType.copyToPasteboard, UIActivityType.addToReadingList, UIActivityType.postToVimeo]
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone {
            self.present(activityViewController, animated: true, completion: nil)
        }
        else {
//            let popoverCntlr = UIPopoverController(contentViewController: activityViewController)
//            popoverCntlr.present(from: CGRect(x: self.view.frame.size.width, y: self.view.frame.size.height, width: 0, height: 0), in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
//
            activityViewController.modalPresentationStyle = .popover
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int { return 1 }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return threeHourForecast != nil ? threeHourForecast!.forecastArray.count : 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForecastCell", for: indexPath) as? ForecastCell {
            //if let dailyForecastData = dailyForecast?.forecastArray[indexPath.row] {
            
            if var dailyForecastData = threeHourForecast?.forecastArray {
                
                if dailyForecastData.count > (indexPath as NSIndexPath).row {
                    
                    cell.forecastData = dailyForecastData[(indexPath as NSIndexPath).row]
                    
                    initAnimations(cell)
                }
            }
            
            return cell
        }
        
        return ForecastCell()
    }
    
    func initAnimations(_ cell: ForecastCell) {
        
        cell.forecastImg.animation = "fadeIn"
        cell.forecastImg.duration = 2
        cell.forecastImg.animate()
        
        cell.weekDayLbl.animation = "fadeInDown"
        cell.weekDayLbl.duration = 1
        cell.weekDayLbl.animate()
        
        
        cell.maxTempLBL.animation = "fadeInRight"
        cell.maxTempLBL.duration = 1
        cell.maxTempLBL.animate()
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let flowLayout = (collectionViewLayout as! UICollectionViewFlowLayout)
        let cellSpacing = flowLayout.minimumInteritemSpacing
        let cellWidth = flowLayout.itemSize.width
        let cellCount = CGFloat(collectionView.numberOfItems(inSection: section))
        
        let collectionViewWidth = collectionView.bounds.size.width
        
        let totalCellWidth = cellCount * cellWidth
        let totalCellSpacing = cellSpacing * (cellCount - 1)
        
        let totalCellsWidth = totalCellWidth + totalCellSpacing
        
        let edgeInsets = (collectionViewWidth - totalCellsWidth) / 2.0
        
        return edgeInsets > 0 ? UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets) : UIEdgeInsetsMake(0, cellSpacing, 0, cellSpacing)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        CityNameTextField.resignFirstResponder()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.text != "" {
            
            curRequestStatus = 2
            
            getWeatherByLabel(textField.text!)
            
            refreshAnimations()
            
        }
        
        textField.resignFirstResponder()
        return true
    }
    
//    @objc func keyboardWillShow(_ sender: Notification) {
//        self.view.frame.origin.y -= 150
//        keyboardStatus = true
//    }
//
//    @objc func keyboardWillHide(_ sender: Notification) {
//        self.view.frame.origin.y = 0
//        keyboardStatus = false
//    }
    
//    @objc func keybordWillHideBackground(_ notification : Notification) {
//        if keyboardStatus == true {
//            CityNameTextField.resignFirstResponder()
//        }
//    }
}
