//
//  InfoViewController.swift
//  OneTapWeather
//
//  Created by Altynbek Orumbaev on 18.01.16.
//  Copyright © 2016 Altynbek Orumbaev. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {
    
    @IBOutlet weak var infoTextFeld: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        infoTextFeld.isScrollEnabled = false
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        infoTextFeld.isScrollEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = curColor
        }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? ViewController {
            vc.getWeatherByName(vc.CityNameTextField.text!, countryName: vc.CountryNameLabel.text!)
        }
    }
}
