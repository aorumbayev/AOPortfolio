//
//  ThreeHourForecast.swift
//  OneTapWeather
//
//  Created by Altynbek Orumbaev on 03.02.16.
//  Copyright © 2016 Altynbek Orumbaev. All rights reserved.
//


import Foundation
import Alamofire

class ThreeHourForecast {

    fileprivate var forecastURL: String!
    
    
    
    init(lattitude: Double, longitude: Double) {
        
        forecastURL = (WEATHER_FORECAST_URL)+"lat=\(lattitude)&lon=\(longitude)" + (API_KEY)
        
    }
    
    init(cityName: String, countryName: String) {
        
        forecastURL = (WEATHER_FORECAST_URL)+"q=\(cityName),\(countryName.lowercased())" + (API_KEY)
        forecastURL = forecastURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) 
    }
    
    init(cityName: String) {
        forecastURL = (WEATHER_FORECAST_URL)+"q=\(cityName)" + (API_KEY)
        forecastURL = forecastURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    }
    
    init(){
        
    }
    
    fileprivate var arrayCount = 16
    
    fileprivate(set) lazy var forecastArray = [ThreeHourForecastData]()
    
    func getDailyForecast(_ completed: @escaping DownloadComplete) {
    
        forecastArray.removeAll()
        
        
        let url  = URL(string: forecastURL as String)!
        
        Alamofire.request(url).responseJSON { response -> Void in
            let result = response.result
            
            if let dict = result.value as? [String : AnyObject] {
                //print(dict)
                
                if let forecastList = dict["list"] as? [[String: AnyObject]] {
                    if forecastList.count < self.arrayCount {
                    self.arrayCount = forecastList.count
                    }
                    for index in 0 ..< self.arrayCount {
                        
                        
                            var listData = forecastList[index]
                            
                            
                        var formattedTime = ""
                        if let utcDateTime = listData["dt"] as? Int {
                            formattedTime = self.getTimeInDesiredFormat(utcDateTime)
                        }
                        
                        var imageId = ""
                        if let weather = listData["weather"] as? [[String: AnyObject]] {
                            if let iconID = weather[0]["icon"] as? String {
                                imageId = iconID
                            }
                        }
                        
                        var temperature:Int!
                        if let main = listData["main"] as? [String:AnyObject] {
                            if let temp = main["temp"] as? NSNumber {
                                temperature = temp.intValue
                            }
                        }
                        
                        let threeHourForecastData = ThreeHourForecastData(
                            dateTime: formattedTime, imageId: imageId, tempK: temperature)
                        
                        self.forecastArray.append(threeHourForecastData)
                        
                    } // end of for loop
                    print("ThreeHourForecast: Data Collected")
                    completed()
                } else {
                    print("ThreeHourForecast: Data Collection Failed")
                }
            }
        }
    }
    
    fileprivate func getTimeInDesiredFormat(_ utcDateTime: Int) -> String {
        let readableDateTime = Date(timeIntervalSince1970: Double(utcDateTime))
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.locale = Locale(identifier: "en_EN")
        dateFormatter.dateFormat = "EEE"
        let dayName = dateFormatter.string(from: readableDateTime)
        
        let localTime = DateFormatter.localizedString(from: readableDateTime,
            dateStyle: .none, timeStyle: .short)
        
        let timeFormatter = DateFormatter()
        var stringToDateFormat = ""
        var dateToStringFormat = ""
        
        if localTime.contains("AM") || localTime.contains("PM") {
            stringToDateFormat = "h:mm a"
            dateToStringFormat = "ha"
        } else {
            stringToDateFormat = "H:mm"
            dateToStringFormat = "H"
        }
        
        timeFormatter.dateFormat = stringToDateFormat
        if let formattedTime = timeFormatter.date(from: localTime) {
            timeFormatter.dateFormat = dateToStringFormat
            let timeInDesiredFormat = timeFormatter.string(from: formattedTime)
            return dayName + " " + timeInDesiredFormat
        }
        print("ThreeHourForecast: TimeFormat Error")
        return "Error"
    }
}
