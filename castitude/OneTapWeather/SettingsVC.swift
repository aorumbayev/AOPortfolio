//
//  SettingsVC.swift
//  OneTapWeather
//
//  Created by Altynbek Orumbaev on 22.02.16.
//  Copyright © 2016 Altynbek Orumbaev. All rights reserved.
//

import UIKit

class SettingsVC: UITableViewController {
    
    @IBOutlet weak var tempSwitch: UISwitch!
    @IBOutlet weak var tempLabel: UILabel!
    
  //  var PurchaseID = "com.aorumbaev.WeatherCastInApp"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.isHidden = false
        
        tempSwitch.setOn(!isMetric, animated: false)
        
        if isMetric{
            tempLabel.text = "Celsius"
        }
        else {
            tempLabel.text = "Fahrenheit"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = curColor
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.textLabel!.textColor = curColor
        header.alpha = 0.7
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = "Settings"
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedStringKey.font: UIFont(name: "Avenir-Book", size: 20)!,  NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return 3
        }
        else {
            return 1
        }
    }
        
    @IBAction func tempChanges(_ sender: UISwitch!) {
        
        if tempLabel.text == "Celsius" {
            tempLabel.text = "Fahrenheit"
            
        }
            
        else {
            tempLabel.text = "Celsius"
        }
        
        isMetric = !sender.isOn
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let tagOfCell = tableView.cellForRow(at: indexPath)?.tag  {
            
            if tagOfCell == 41 {
                rateApp()
                tableView.deselectRow(at: indexPath, animated: true)
            }
            
            if tagOfCell == 42 {
                openFacebookPage()
                tableView.deselectRow(at: indexPath, animated: true)
            }
            
            if tagOfCell == 43 {
                openInstagramPage()
                tableView.deselectRow(at: indexPath, animated: true)
            }
            
            if tagOfCell == 46 {
                awardyRateApp()
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }
    
    
    func showErrorAlert(_ title: String, msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    func rateApp(){
        let storeURL = URL(string: "itms-apps://itunes.apple.com/app/id1071004700")!
        if UIApplication.shared.canOpenURL(storeURL) {
            UIApplication.shared.openURL(storeURL)
        }
    }
    
    func awardyRateApp(){
        let storeURL = URL(string: "itms-apps://itunes.apple.com/app/id1076012055")!
        if UIApplication.shared.canOpenURL(storeURL) {
            UIApplication.shared.openURL(storeURL)
        }
    }
    
    func openFacebookPage() {
        UIApplication.shared.openURL(URL(string: "https://www.facebook.com/castitudeAPP")!)
    }
    
    func openInstagramPage() {
        let instagramURL = URL(string: "instagram://user?username=castitude")!
        if UIApplication.shared.canOpenURL(instagramURL) {
            UIApplication.shared.openURL(instagramURL)
        } else {
            UIApplication.shared.openURL(URL(string: "https://www.instagram.com/castitude/")!)
        }
    }
    
    
//    @IBAction func disableAdsBtnPressed(sender: AnyObject) {
//        
//        NetworkActivityIndicatorManager.networkOperationStarted()
//        SwiftyStoreKit.purchaseProduct(PurchaseID) { result in
//            NetworkActivityIndicatorManager.networkOperationFinished()
//            
//            self.showAlert(self.alertForPurchaseResult(result))
//        }
//        
//    }
//    
//    @IBAction func restorePurchasesBtnPressed(sender: AnyObject) {
//        NetworkActivityIndicatorManager.networkOperationStarted()
//        SwiftyStoreKit.restorePurchases() { result in
//            NetworkActivityIndicatorManager.networkOperationFinished()
//            
//            self.showAlert(self.alertForRestorePurchases(result))
//        }
//        
//    }
    
}

//extension SettingsVC {
//    
//    func alertWithTitle(title: String, message: String) -> UIAlertController {
//        
//        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
//        alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
//        return alert
//    }
//    
//    func showAlert(alert: UIAlertController) {
//        guard let _ = self.presentedViewController else {
//            self.presentViewController(alert, animated: true, completion: nil)
//            return
//        }
//    }
//    
//    func alertForPurchaseResult(result: SwiftyStoreKit.PurchaseResult) -> UIAlertController {
//        switch result {
//        case .Success(let productId):
//            print("Purchase Success: \(productId)")
//            NSUserDefaults.standardUserDefaults().setValue("true", forKey: "purchased")
//            return alertWithTitle("Thank You", message: "Purchase completed")
//        case .Error(let error):
//            print("Purchase Failed: \(error)")
//            switch error {
//            case .Failed(let error):
//                if case ResponseError.RequestFailed(let internalError) = error where internalError.domain == SKErrorDomain {
//                    return alertWithTitle("Purchase failed", message: "Please check your Internet connection or try again later")
//                }
//                if (error as NSError).domain == SKErrorDomain {
//                    return alertWithTitle("Purchase failed", message: "Please check your Internet connection or try again later")
//                }
//                return alertWithTitle("Purchase failed", message: "Unknown error. Please contact support")
//            case .NoProductIdentifier:
//                return alertWithTitle("Purchase failed", message: "Product not found")
//            case .PaymentNotAllowed:
//                return alertWithTitle("Payments not enabled", message: "You are not allowed to make payments")
//            }
//        }
//    }
//    
//    func alertForRestorePurchases(result: SwiftyStoreKit.RestoreResult) -> UIAlertController {
//        
//        switch result {
//        case .Success(let productId):
//            print("Restore Success: \(productId)")
//            NSUserDefaults.standardUserDefaults().setValue("true", forKey: "purchased")
//            return alertWithTitle("Purchases Restored", message: "All purchases have been restored")
//        case .NothingToRestore:
//            print("Nothing to Restore")
//            return alertWithTitle("Nothing to restore", message: "No previous purchases were found")
//        case .Error(let error):
//            print("Restore Failed: \(error)")
//            return alertWithTitle("Restore failed", message: "Unknown error. Please contact support")
//        }
//    }
//    
//}
