//
//  MaterialImageView.swift
//  OneTapWeather
//
//  Created by Altynbek Orumbaev on 21.12.15.
//  Copyright © 2015 Altynbek Orumbaev. All rights reserved.
//

import UIKit
import Spring

class MaterialImageView: SpringImageView {

    
    
    override func awakeFromNib() {
       
        self.animation = "fadeInRight"
        self.animate()

    }
    
    
}
