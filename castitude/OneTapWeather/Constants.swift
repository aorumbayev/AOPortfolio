//
//  Constants.swift
//  OneTapWeather
//
//  Created by Altynbek Orumbaev on 19.12.15.
//  Copyright © 2015 Altynbek Orumbaev. All rights reserved.
//

import Foundation
import UIKit

var WEATHER_URL_BY_NAME = "http://api.openweathermap.org/data/2.5/weather?q="
var WEATHER_URL_BY_COORDINATES_LATTITUDE = "http://api.openweathermap.org/data/2.5/weather?lat="
var WEATHER_URL_BY_COORDINATES_LONGITUDE = "&lon="

var WEATHER_FORECAST_URL = "http://api.openweathermap.org/data/2.5/forecast?"

var API_KEY = "&appid=7ff7443b07ddffb620c5071bc4b391e5"

let SHADOW_COLOR: CGFloat = 0.0 / 255.0

var isMetric = true

let weatherImageDict = [
    "01d" : "01d",
    "01n" : "01n",
    
    "02d" : "02d",
    "02n" : "02n",
    
    "03d" : "03d",
    "03n" : "03n",
    
    "04d" : "04d",
    "04n" : "04n",
    
    "09d" : "09d",
    "09n" : "09n",
    
    "10d" : "10d",
    "10n" : "10n",
    
    "11d" : "11d",
    "11n" : "11n",
    
    "13d" : "13d",
    "13n" : "13n",
    
    "50d" : "50d",
    "50n" : "50n"]

var curColor = UIColor.init(red: 65.0/255.0, green: 179/255.0, blue: 247/255.0, alpha: 1.0)

let colorsDictionary:Dictionary<String,UIColor> = [
    "Cold": UIColor.init(red: 65.0/255.0, green: 179/255.0, blue: 247/255.0, alpha: 1.0),
    "Middle":UIColor.init(red: 0.0/255.0, green: 187/255.0, blue: 163/255.0, alpha: 1.0),
    "Hot":UIColor.init(red: 255.0/255.0, green: 152/255.0, blue: 0/255.0, alpha: 1.0),
    "White":UIColor.init(red: 255.0, green: 255.0, blue: 255.0, alpha: 1.0)]

typealias DownloadComplete = () -> ()

func convertTemperature(_ temperature: Int) -> Int {
    return isMetric ? temperature - 273 : Int(1.8 * Double(temperature - 273) + 32)
}

struct ThreeHourForecastData {
    fileprivate(set) var dateWithTime: String!
    fileprivate(set) var imageIconId: String!
    fileprivate var temperatureK: Int!
    
    var temperature: String! {
        return "\(convertTemperature(temperatureK))"
    }
    
    init(dateTime: String, imageId: String, tempK: Int) {
        self.dateWithTime = dateTime
        self.imageIconId = imageId
        self.temperatureK = tempK
    }
}
