//
//  ForecastCell.swift
//  OneTapWeather
//
//  Created by Altynbek Orumbaev on 27.01.16.
//  Copyright © 2016 Altynbek Orumbaev. All rights reserved.
//

import UIKit

import Spring

class ForecastCell: UICollectionViewCell {
    
    @IBOutlet weak var weekDayLbl: SpringLabel!
    @IBOutlet weak var forecastImg: SpringImageView!
    @IBOutlet weak var maxTempLBL: SpringLabel!
   
    var forecastData: ThreeHourForecastData! {
        didSet {
            self.weekDayLbl.text = forecastData.dateWithTime
            self.forecastImg.image = UIImage(named: weatherImageDict["\(forecastData.imageIconId!)"]!)
            self.maxTempLBL.text = forecastData.temperature + "°"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
