//
//  OneTapWeatherUITests.swift
//  OneTapWeatherUITests
//
//  Created by Altynbek Orumbaev on 19.12.15.
//  Copyright © 2015 Altynbek Orumbaev. All rights reserved.
//

import XCTest
import AppCenterXCUITestExtensions

class OneTapWeatherUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        ACTLaunch.launch();

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        XCUIApplication().terminate()
    }
    
    func testExample2() {
        
        let app = XCUIApplication()
        app.buttons["locationIcon"].tap()
        app.buttons["refreshIcon"].tap()
        app.buttons["shareIcon"].tap()
        app.buttons["Cancel"].tap()
        
        let collectionView = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .collectionView).element
        collectionView.swipeLeft()
        collectionView.swipeRight()
        collectionView.swipeLeft()
        
        let element = app.collectionViews.cells.children(matching: .other).element.children(matching: .other).element
        element.tap()
        element.tap()
        app/*@START_MENU_TOKEN@*/.scrollViews/*[[".otherElements[\"Home screen icons\"]",".otherElements[\"SBFolderScalingView\"].scrollViews",".scrollViews"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.otherElements.icons["Castitude"].tap()
        
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        
        let app = ACTLaunch.launch(XCUIApplication())!
        let element = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        element.children(matching: .other).element(boundBy: 2).children(matching: .button).element(boundBy: 0).tap()
        
        let collectionViewsQuery = app.collectionViews
        collectionViewsQuery.children(matching: .cell).element(boundBy: 3).children(matching: .other).element.children(matching: .other).element.swipeLeft()
        element.children(matching: .other).element(boundBy: 1).children(matching: .collectionView).element.tap()
        
        let element2 = collectionViewsQuery.children(matching: .cell).element(boundBy: 4).children(matching: .other).element.children(matching: .other).element
        element2.swipeLeft()
        element2.swipeLeft()
        element2.swipeLeft()
        element.children(matching: .other).element(boundBy: 0).children(matching: .button).element(boundBy: 1).tap()
        
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
}
